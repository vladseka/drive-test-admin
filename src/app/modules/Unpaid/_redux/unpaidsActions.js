import * as requestFromServer from "./unpaidsCrud";
import { unpaidsSlice, callTypes } from "./unpaidsSlice";

const { actions } = unpaidsSlice;

export const fetchUnpaids = queryParams => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.list }));
    return requestFromServer
        .findUnpaids(queryParams)
        .then(response => {
            const { totalCount, entities } = response.data;
            dispatch(actions.unpaidsFetched({ totalCount, entities }));
        })
        .catch(error => {
            error.clientMessage = "Can't find unpaids";
            dispatch(actions.catchError({ error, callType: callTypes.list }));
        });
};

export const updateUnpaidsStatus = (ids) => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.action }));
    return requestFromServer
        .updateStatusForUnpaids(ids)
        .then(() => {
            dispatch(actions.unpaidsStatusUpdated({ ids }));
        })
        .catch(error => {
            error.clientMessage = "Can't update payment status";
            dispatch(actions.catchError({ error, callType: callTypes.action }));
        });
};