import axios from "axios";

export const ORDERS_URL = process.env.REACT_APP_SERVER_URL + "/api/orders";
// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findUnpaids(queryParams) {
    return axios.post(`${ORDERS_URL}/find`, { queryParams });
}

// UPDATE Status
export function updateStatusForUnpaids(ids) {
    return axios.post(`${ORDERS_URL}/approvepayment`, {
        ids
    });
}
