import { createSlice } from "@reduxjs/toolkit";

const initialUnpaidsState = {
    listLoading: false,
    actionsLoading: false,
    totalCount: 0,
    entities: null,
    unpaidForEdit: undefined,
    lastError: null
};
export const callTypes = {
    list: "list",
    action: "action"
};

export const unpaidsSlice = createSlice({
    name: "unpaids",
    initialState: initialUnpaidsState,
    reducers: {
        catchError: (state, action) => {
            state.error = `${action.type}: ${action.payload.error}`;
            if (action.payload.callType === callTypes.list) {
                state.listLoading = false;
            } else {
                state.actionsLoading = false;
            }
        },
        startCall: (state, action) => {
            state.error = null;
            if (action.payload.callType === callTypes.list) {
                state.listLoading = true;
            } else {
                state.actionsLoading = true;
            }
        },
        // findUnpaids
        unpaidsFetched: (state, action) => {
            const { totalCount, entities } = action.payload;
            state.listLoading = false;
            state.error = null;
            state.entities = entities;
            state.totalCount = totalCount;
        },
        // unpaidsUpdateState
        unpaidsStatusUpdated: (state, action) => {
            state.actionsLoading = false;
            state.error = null;
        }
    }
});
