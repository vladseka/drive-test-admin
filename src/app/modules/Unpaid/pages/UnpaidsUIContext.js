import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./UnpaidsUIHelpers";

const UnpaidsUIContext = createContext();

export function useUnpaidsUIContext() {
    return useContext(UnpaidsUIContext);
}

export const UnpaidsUIConsumer = UnpaidsUIContext.Consumer;

export function UnpaidsUIProvider({ unpaidsUIEvents, children }) {
    const [queryParams, setQueryParamsBase] = useState(initialFilter);
    const [ids, setIds] = useState([]);
    const setQueryParams = useCallback((nextQueryParams) => {
        setQueryParamsBase((prevQueryParams) => {
            if (isFunction(nextQueryParams)) {
                nextQueryParams = nextQueryParams(prevQueryParams);
            }

            if (isEqual(prevQueryParams, nextQueryParams)) {
                return prevQueryParams;
            }

            return nextQueryParams;
        });
    }, []);

    const value = {
        queryParams,
        setQueryParamsBase,
        ids,
        setIds,
        setQueryParams,
        openUpdateUnpaidsStatusDialog:
            unpaidsUIEvents.openUpdateUnpaidsStatusDialog,
    };

    return (
        <UnpaidsUIContext.Provider value={value}>
            {children}
        </UnpaidsUIContext.Provider>
    );
}
