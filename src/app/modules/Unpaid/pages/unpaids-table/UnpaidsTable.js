// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
    PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../_redux/unpaidsActions";
import * as uiHelpers from "../UnpaidsUIHelpers";
import {
    getSelectRow,
    getHandlerTableChange,
    NoRecordsFoundMessage,
    PleaseWaitMessage,
    sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useUnpaidsUIContext } from "../UnpaidsUIContext";

export function UnpaidsTable() {
    // Unpaids UI Context
    const unpaidsUIContext = useUnpaidsUIContext();
    const unpaidsUIProps = useMemo(() => {
        return {
            ids: unpaidsUIContext.ids,
            setIds: unpaidsUIContext.setIds,
            queryParams: unpaidsUIContext.queryParams,
            setQueryParams: unpaidsUIContext.setQueryParams,
        };
    }, [unpaidsUIContext]);

    // Getting curret state of unpaids list from store (Redux)
    const { currentState } = useSelector(
        (state) => ({ currentState: state.unpaids }),
        shallowEqual
    );
    const { totalCount, entities, listLoading } = currentState;
    // Unpaids Redux state
    const dispatch = useDispatch();
    useEffect(() => {
        // clear selections list
        unpaidsUIProps.setIds([]);
        // server call by queryParams
        dispatch(actions.fetchUnpaids(unpaidsUIProps.queryParams));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [unpaidsUIProps.queryParams, dispatch]);
    // Table columns
    const columns = [
        {
            dataField: "testNumber",
            text: "Unpaid ID",
            formatter: columnFormatters.IdColumnFormatter,
        },
        {
            dataField: "name",
            text: "Name",
            formatter: columnFormatters.NameColumnFormatter,
        },
        {
            dataField: "phone",
            text: "Mobile No.",
        },
        {
            dataField: "date",
            text: "Date",
            formatter: columnFormatters.DateColumnFormatter
        },
        {
            dataField: "type",
            text: "Package",
            sort: true,
            sortCaret: sortCaret,
            formatter: columnFormatters.PackageColumnFormatter,
        },
        {
            dataField: "paid",
            text: "Status",
            sort: true,
            sortCaret: sortCaret,
            formatter: columnFormatters.StatusColumnFormatter,
        },
    ];
    // Table pagination properties
    const paginationOptions = {
        custom: true,
        totalSize: totalCount,
        sizePerPageList: uiHelpers.sizePerPageList,
        sizePerPage: unpaidsUIProps.queryParams.pageSize,
        page: unpaidsUIProps.queryParams.pageNumber,
    };
    return (
        <>
            <PaginationProvider pagination={paginationFactory(paginationOptions)}>
                {({ paginationProps, paginationTableProps }) => {
                    return (
                        <Pagination
                            isLoading={listLoading}
                            paginationProps={paginationProps}
                        >
                            <BootstrapTable
                                wrapperClasses="table-responsive"
                                classes="table table-head-custom table-vertical-center"
                                bootstrap4
                                bordered={false}
                                remote
                                keyField="testNumber"
                                data={entities === null ? [] : entities}
                                columns={columns}
                                defaultSorted={uiHelpers.defaultSorted}
                                onTableChange={getHandlerTableChange(
                                    unpaidsUIProps.setQueryParams
                                )}
                                selectRow={getSelectRow({
                                    entities,
                                    ids: unpaidsUIProps.ids,
                                    setIds: unpaidsUIProps.setIds,
                                })}
                                {...paginationTableProps}
                            >
                                <PleaseWaitMessage entities={entities} />
                                <NoRecordsFoundMessage entities={entities} />
                            </BootstrapTable>
                        </Pagination>
                    );
                }}
            </PaginationProvider>
        </>
    );
}
