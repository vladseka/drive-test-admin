import React from "react";
import {
  UnpaidPackageCssClasses,
  UnpaidPackageTitles
} from "../../UnpaidsUIHelpers";

export const PackageColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        UnpaidPackageCssClasses[row.type]
        } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        UnpaidPackageCssClasses[row.type]
        }`}
    >
      {UnpaidPackageTitles[row.type]}
    </span>
  </>
);
