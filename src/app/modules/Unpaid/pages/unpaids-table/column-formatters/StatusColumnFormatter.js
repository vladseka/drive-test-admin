import React from "react";
import {
  UnpaidStatusCssClasses,
  UnpaidStatusTitles
} from "../../UnpaidsUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      UnpaidStatusCssClasses[0]
      } label-inline`}
  >
    {UnpaidStatusTitles[0]}
  </span>
);
