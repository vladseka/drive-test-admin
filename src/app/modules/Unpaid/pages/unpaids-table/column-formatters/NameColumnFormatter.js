import React from "react";
import { upperCase } from "lodash";

export const NameColumnFormatter = (cellContent, row) => {
    let name = row.name;
    name = upperCase(name);
    return (<>{name}</>)
}
