import React, { useMemo } from "react";
import { useUnpaidsUIContext } from "../UnpaidsUIContext";

export function UnpaidsGrouping() {
  // Unpaids UI Context
  const unpaidsUIContext = useUnpaidsUIContext();
  const unpaidsUIProps = useMemo(() => {
    return {
      ids: unpaidsUIContext.ids,
      setIds: unpaidsUIContext.setIds,
      openUpdateUnpaidsStatusDialog:
        unpaidsUIContext.openUpdateUnpaidsStatusDialog,
    };
  }, [unpaidsUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{unpaidsUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={unpaidsUIProps.openUpdateUnpaidsStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Approve Payment
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
