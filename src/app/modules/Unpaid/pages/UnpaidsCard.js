import React, { useMemo } from "react";
import {
    Card,
    CardBody,
    CardHeader,
    CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { UnpaidsFilter } from "./unpaids-filter/UnpaidsFilter";
import { UnpaidsTable } from "./unpaids-table/UnpaidsTable";
import { UnpaidsGrouping } from "./unpaids-grouping/UnpaidsGrouping";
import { useUnpaidsUIContext } from "./UnpaidsUIContext";

export function UnpaidsCard() {
    const unpaidsUIContext = useUnpaidsUIContext();
    const unpaidsUIProps = useMemo(() => {
        return {
            ids: unpaidsUIContext.ids,
            queryParams: unpaidsUIContext.queryParams,
            setQueryParams: unpaidsUIContext.setQueryParams,
            openUpdateUnpaidsStatusDialog:
                unpaidsUIContext.openUpdateUnpaidsStatusDialog,
        };
    }, [unpaidsUIContext]);

    return (
        <Card>
            <CardHeader title="Unpaid Order List">
                <CardHeaderToolbar>
                    {/* <button
                        type="button"
                        className="btn btn-primary"
                        onClick={unpaidsUIProps.newUnpaidButtonClick}
                    >
                        New Unpaid
          </button> */}
                </CardHeaderToolbar>
            </CardHeader>
            <CardBody>
                <UnpaidsFilter />
                {unpaidsUIProps.ids.length > 0 && (
                    <>
                        <UnpaidsGrouping />
                    </>
                )}
                <UnpaidsTable />
            </CardBody>
        </Card>
    );
}
