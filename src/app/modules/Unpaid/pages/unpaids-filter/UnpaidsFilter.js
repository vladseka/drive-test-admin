import React, { useMemo } from "react";
import { Formik } from "formik";
import { isEqual } from "lodash";
import { useUnpaidsUIContext } from "../UnpaidsUIContext";

const prepareFilter = (queryParams, values) => {
  const { searchText } = values;
  const newQueryParams = { ...queryParams };
  const filter = {};
  // Filter by all fields
  filter.id = searchText;
  filter.paid = 0;
  if (searchText) {
    filter.name = searchText;
    filter.phone = searchText;
  }
  newQueryParams.filter = filter;
  return newQueryParams;
};

export function UnpaidsFilter({ listLoading }) {
  // Unpaids UI Context
  const unpaidsUIContext = useUnpaidsUIContext();
  const unpaidsUIProps = useMemo(() => {
    return {
      setQueryParams: unpaidsUIContext.setQueryParams,
      queryParams: unpaidsUIContext.queryParams,
    };
  }, [unpaidsUIContext]);

  const applyFilter = (values) => {
    const newQueryParams = prepareFilter(unpaidsUIProps.queryParams, values);
    if (!isEqual(newQueryParams, unpaidsUIProps.queryParams)) {
      newQueryParams.pageNumber = 1;
      unpaidsUIProps.setQueryParams(newQueryParams);
    }
  };

  return (
    <>
      <Formik
        initialValues={{
          paid: 1,
          searchText: "",
        }}
        onSubmit={(values) => {
          applyFilter(values);
        }}
      >
        {({
          values,
          handleSubmit,
          handleBlur,
          handleChange,
          setFieldValue,
        }) => (
            <form onSubmit={handleSubmit} className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-2">
                  <input
                    type="text"
                    className="form-control"
                    name="searchText"
                    placeholder="Search"
                    onBlur={handleBlur}
                    value={values.searchText}
                    onChange={(e) => {
                      setFieldValue("searchText", e.target.value);
                      handleSubmit();
                    }}
                  />
                  <small className="form-text text-muted">
                    <b>Search</b> in all fields
                </small>
                </div>
              </div>
            </form>
          )}
      </Formik>
    </>
  );
}
