import React from "react";
import { Route } from "react-router-dom";
import { UnpaidsLoadingDialog } from "./unpaids-loading-dialog/UnpaidsLoadingDialog";
import { UnpaidsUpdateStatusDialog } from "./unpaids-update-status-dialog/UnpaidsUpdateStatusDialog";
import { UnpaidsCard } from "./UnpaidsCard";
import { UnpaidsUIProvider } from "./UnpaidsUIContext";

export function UnpaidsPage({ history }) {
    const unpaidsUIEvents = {
        openUpdateUnpaidsStatusDialog: () => {
            history.push("/unpaids/updateStatus");
        },
    };

    return (
        <UnpaidsUIProvider unpaidsUIEvents={unpaidsUIEvents}>
            <UnpaidsLoadingDialog />
            <Route path="/unpaids/updateStatus">
                {({ history, match }) => (
                    <UnpaidsUpdateStatusDialog
                        show={match != null}
                        onHide={() => {
                            history.push("/unpaids");
                        }}
                    />
                )}
            </Route>
            <UnpaidsCard />
        </UnpaidsUIProvider>
    );
}
