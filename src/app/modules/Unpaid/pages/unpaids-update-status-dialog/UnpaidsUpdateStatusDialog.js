import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { UnpaidStatusCssClasses } from "../UnpaidsUIHelpers";
import * as actions from "../../_redux/unpaidsActions";
import { useUnpaidsUIContext } from "../UnpaidsUIContext";

const selectedUnpaids = (entities, ids) => {
  const _unpaids = [];
  ids.forEach((id) => {
    const unpaid = entities.find((el) => el.id === id);
    if (unpaid) {
      _unpaids.push(unpaid);
    }
  });
  return _unpaids;
};

export function UnpaidsUpdateStatusDialog({ show, onHide }) {
  // Unpaids UI Context
  const unpaidsUIContext = useUnpaidsUIContext();
  const unpaidsUIProps = useMemo(() => {
    return {
      ids: unpaidsUIContext.ids,
      setIds: unpaidsUIContext.setIds,
      queryParams: unpaidsUIContext.queryParams,
    };
  }, [unpaidsUIContext]);

  // Unpaids Redux state
  const { unpaids, isLoading } = useSelector(
    (state) => ({
      unpaids: selectedUnpaids(state.unpaids.entities, unpaidsUIProps.ids),
      isLoading: state.unpaids.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected unpaids we should close modal
  useEffect(() => {
    if (unpaidsUIProps.ids || unpaidsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [unpaidsUIProps.ids]);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing unpaid by ids
    dispatch(actions.updateUnpaidsStatus(unpaidsUIProps.ids)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchUnpaids(unpaidsUIProps.queryParams)).then(
          () => {
            // clear selections list
            unpaidsUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Payment has been approved for selected orders
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {unpaids.map((unpaid) => (
              <div className="list-timeline-item mb-3" key={unpaid.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      UnpaidStatusCssClasses[unpaid.paid]
                      } label-inline`}
                    style={{ width: "60px" }}
                  >
                    {unpaid.name}
                  </span>{" "}
                  <span className="ml-5">
                    {unpaid.phone}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Approve Payment
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
