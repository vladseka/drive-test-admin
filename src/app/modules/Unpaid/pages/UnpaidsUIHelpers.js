export const UnpaidStatusCssClasses = ["warning", ""];
export const UnpaidStatusTitles = ["No Payment"];
export const UnpaidPackageCssClasses = ["success", "info", ""];
export const UnpaidPackageTitles = ["One Time", "Life Time"];
export const defaultSorted = [{ dataField: "id", order: "asc" }];
export const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
];
export const initialFilter = {
    filter: {
        name: "",
        phone: "",
        paid: false
    },
    sortOrder: "asc", // asc||desc
    sortField: "id",
    pageNumber: 1,
    pageSize: 10
};