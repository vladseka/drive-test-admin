import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./RefundsUIHelpers";

const RefundsUIContext = createContext();

export function useRefundsUIContext() {
    return useContext(RefundsUIContext);
}

export const RefundsUIConsumer = RefundsUIContext.Consumer;

export function RefundsUIProvider({ refundsUIEvents, children }) {
    const [queryParams, setQueryParamsBase] = useState(initialFilter);
    const [ids, setIds] = useState([]);
    const setQueryParams = useCallback((nextQueryParams) => {
        setQueryParamsBase((prevQueryParams) => {
            if (isFunction(nextQueryParams)) {
                nextQueryParams = nextQueryParams(prevQueryParams);
            }

            if (isEqual(prevQueryParams, nextQueryParams)) {
                return prevQueryParams;
            }

            return nextQueryParams;
        });
    }, []);

    const value = {
        queryParams,
        setQueryParamsBase,
        ids,
        setIds,
        setQueryParams,
    };

    return (
        <RefundsUIContext.Provider value={value}>
            {children}
        </RefundsUIContext.Provider>
    );
}
