import React from "react";
import {
    Card,
    CardBody,
    CardHeader,
    CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { RefundsFilter } from "./refunds-filter/RefundsFilter";
import { RefundsTable } from "./refunds-table/RefundsTable";

export function RefundsCard({ status }) {

    return (
        <Card>
            <CardHeader title="Refunds History">
                <CardHeaderToolbar>
                </CardHeaderToolbar>
            </CardHeader>
            <CardBody>
                <RefundsFilter />
                <RefundsTable />
            </CardBody>
        </Card>
    );
}
