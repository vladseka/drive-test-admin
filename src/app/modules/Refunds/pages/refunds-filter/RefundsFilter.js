import React, { useMemo } from "react";
import { Formik } from "formik";
import { isEqual } from "lodash";
import { useRefundsUIContext } from "../RefundsUIContext";

const prepareFilter = (queryParams, values) => {
  const { status, type, searchText } = values;
  const newQueryParams = { ...queryParams };
  const filter = {};
  // Filter by all fields
  filter.id = searchText;
  filter.refund = 1;
  if (searchText) {
    filter.name = searchText;
    filter.phone = searchText;
  }
  newQueryParams.filter = filter;
  return newQueryParams;
};

export function RefundsFilter({ listLoading }) {
  // Refunds UI Context
  const refundsUIContext = useRefundsUIContext();
  const refundsUIProps = useMemo(() => {
    return {
      setQueryParams: refundsUIContext.setQueryParams,
      queryParams: refundsUIContext.queryParams,
    };
  }, [refundsUIContext]);

  const applyFilter = (values) => {
    const newQueryParams = prepareFilter(refundsUIProps.queryParams, values);
    if (!isEqual(newQueryParams, refundsUIProps.queryParams)) {
      newQueryParams.pageNumber = 1;
      refundsUIProps.setQueryParams(newQueryParams);
    }
  };

  return (
    <>
      <Formik
        initialValues={{
          refund: 1,
          searchText: "",
        }}
        onSubmit={(values) => {
          applyFilter(values);
        }}
      >
        {({
          values,
          handleSubmit,
          handleBlur,
          handleChange,
          setFieldValue,
        }) => (
            <form onSubmit={handleSubmit} className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-2">
                  <input
                    type="text"
                    className="form-control"
                    name="searchText"
                    placeholder="Search"
                    onBlur={handleBlur}
                    value={values.searchText}
                    onChange={(e) => {
                      setFieldValue("searchText", e.target.value);
                      handleSubmit();
                    }}
                  />
                  <small className="form-text text-muted">
                    <b>Search</b> in all fields
                </small>
                </div>
              </div>
            </form>
          )}
      </Formik>
    </>
  );
}
