import React from "react";
import { RefundsLoadingDialog } from "./refunds-loading-dialog/RefundsLoadingDialog";
import { RefundsCard } from "./RefundsCard";
import { RefundsUIProvider } from "./RefundsUIContext";

export function RefundsPage({ history }) {

    return (
        <RefundsUIProvider>
            <RefundsLoadingDialog />
            <RefundsCard />
        </RefundsUIProvider>
    );
}
