// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
    PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../_redux/refundsActions";
import * as uiHelpers from "../RefundsUIHelpers";
import {
    getHandlerTableChange,
    NoRecordsFoundMessage,
    PleaseWaitMessage,
    sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useRefundsUIContext } from "../RefundsUIContext";

export function RefundsTable() {
    // Refunds UI Context
    const refundsUIContext = useRefundsUIContext();
    const refundsUIProps = useMemo(() => {
        return {
            ids: refundsUIContext.ids,
            setIds: refundsUIContext.setIds,
            queryParams: refundsUIContext.queryParams,
            setQueryParams: refundsUIContext.setQueryParams,
        };
    }, [refundsUIContext]);

    // Getting curret state of refunds list from store (Redux)
    const { currentState } = useSelector(
        (state) => ({ currentState: state.refunds }),
        shallowEqual
    );
    const { totalCount, entities, listLoading } = currentState;
    // Refunds Redux state
    const dispatch = useDispatch();
    useEffect(() => {
        // clear selections list
        refundsUIProps.setIds([]);
        // server call by queryParams
        dispatch(actions.fetchRefunds(refundsUIProps.queryParams));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [refundsUIProps.queryParams, dispatch]);
    // Table columns
    const columns = [
        {
            dataField: "testNumber",
            text: "Order ID",
            formatter: columnFormatters.IdColumnFormatter,
        },
        {
            dataField: "name",
            text: "Name",
            formatter: columnFormatters.NameColumnFormatter,
        },
        {
            dataField: "refundDate",
            text: "Date",
            sort: true,
            sortCaret: sortCaret,
            formatter: columnFormatters.DateColumnFormatter,
        },
        {
            dataField: "phone",
            text: "Mobile No.",
        },
        {
            dataField: "refundAmount",
            text: "Refund Amount",
            formatter: columnFormatters.AmountColumnFormatter
        },
        {
            dataField: "refund",
            text: "Status",
            formatter: columnFormatters.StatusColumnFormatter,
        },
    ];
    // Table pagination properties
    const paginationOptions = {
        custom: true,
        totalSize: totalCount,
        sizePerPageList: uiHelpers.sizePerPageList,
        sizePerPage: refundsUIProps.queryParams.pageSize,
        page: refundsUIProps.queryParams.pageNumber,
    };
    return (
        <>
            <PaginationProvider pagination={paginationFactory(paginationOptions)}>
                {({ paginationProps, paginationTableProps }) => {
                    return (
                        <Pagination
                            isLoading={listLoading}
                            paginationProps={paginationProps}
                        >
                            <BootstrapTable
                                wrapperClasses="table-responsive"
                                classes="table table-head-custom table-vertical-center"
                                bootstrap4
                                bordered={false}
                                remote
                                keyField="testNumber"
                                data={entities === null ? [] : entities}
                                columns={columns}
                                defaultSorted={uiHelpers.defaultSorted}
                                onTableChange={getHandlerTableChange(
                                    refundsUIProps.setQueryParams
                                )}
                                {...paginationTableProps}
                            >
                                <PleaseWaitMessage entities={entities} />
                                <NoRecordsFoundMessage entities={entities} />
                            </BootstrapTable>
                        </Pagination>
                    );
                }}
            </PaginationProvider>
        </>
    );
}
