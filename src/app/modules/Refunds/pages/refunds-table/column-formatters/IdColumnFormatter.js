import React from "react";

export const IdColumnFormatter = (cellContent, row) => {
    let id = row.testNumber;
    id = id.substring(id.length - 8, id.length)
    return (<>S1#{id}</>)
}
