import React from "react";
import {
  RefundStatusCssClasses,
  RefundStatusTitles
} from "../../RefundsUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      RefundStatusCssClasses[0]
      } label-inline`}
  >
    {RefundStatusTitles[0]}
  </span>
);
