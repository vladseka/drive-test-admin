import * as requestFromServer from "./refundsCrud";
import { refundsSlice, callTypes } from "./refundsSlice";

const { actions } = refundsSlice;

export const fetchRefunds = queryParams => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.list }));
    return requestFromServer
        .findRefunds(queryParams)
        .then(response => {
            const { totalCount, entities } = response.data;
            dispatch(actions.refundsFetched({ totalCount, entities }));
        })
        .catch(error => {
            error.clientMessage = "Can't find refunds";
            dispatch(actions.catchError({ error, callType: callTypes.list }));
        });
};