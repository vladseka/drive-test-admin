import React from "react";
import { StatsWidgetOrderTotal } from "./widgets/stats/StatsWidgetOrderTotal";
import { StatsWidgetOrderPending } from "./widgets/stats/StatsWidgetOrderPending";
import { StatsWidgetOrderApproved } from "./widgets/stats/StatsWidgetOrderApproved";
import { StatsWidgetOrderCompleted } from "./widgets/stats/StatsWidgetOrderCompleted";
import { StatsWidgetOrderRefunded } from "./widgets/stats/StatsWidgetOrderRefunded";
import { StatsWidgetOrderUnpaid } from "./widgets/stats/StatsWidgetOrderUnpaid";
import { StatsWidgetUserTotal } from "./widgets/stats/StatsWidgetUserTotal";
import { StatsWidgetAffiliateTotal } from "./widgets/stats/StatsWidgetAffiliateTotal";
export function Dashboard() {
    return (
        <div className="row">
            <div className="col-lg-6 col-xxl-4">
                <div>
                    <StatsWidgetOrderTotal className="card-stretch card-stretch-half gutter-b" />
                </div>
            </div>
            <div className="col-lg-6 col-xxl-4">
                <div>
                    <StatsWidgetOrderPending className="card-stretch card-stretch-half gutter-b" />
                </div>
            </div>
            <div className="col-lg-6 col-xxl-4">
                <div>
                    <StatsWidgetOrderApproved className="card-stretch card-stretch-half gutter-b" />
                </div>
            </div>
            <div className="col-lg-6 col-xxl-4">
                <div>
                    <StatsWidgetOrderCompleted className="card-stretch card-stretch-half gutter-b" />
                </div>
            </div>
            <div className="col-lg-6 col-xxl-4">
                <div>
                    <StatsWidgetOrderRefunded className="card-stretch card-stretch-half gutter-b" />
                </div>
            </div>
            <div className="col-lg-6 col-xxl-4">
                <div>
                    <StatsWidgetOrderUnpaid className="card-stretch card-stretch-half gutter-b" />
                </div>
            </div>
            <div className="col-lg-6 col-xxl-4">
                <div>
                    <StatsWidgetUserTotal className="card-stretch card-stretch-half gutter-b" />
                </div>
            </div>
            <div className="col-lg-6 col-xxl-4">
                <div>
                    <StatsWidgetAffiliateTotal className="card-stretch card-stretch-half gutter-b" />
                </div>
            </div>
        </div>
    );
}
