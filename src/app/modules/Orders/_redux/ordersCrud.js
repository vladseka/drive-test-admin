import axios from "axios";

export const ORDERS_URL = process.env.REACT_APP_SERVER_URL + "/api/orders";
export const CENTERS_URL = process.env.REACT_APP_SERVER_URL + "/api/center";

// CREATE =>  POST: add a new order to the server
export function createOrder(order) {
    return axios.post(ORDERS_URL, { order });
}

// READ
export function getAllOrders() {
    return axios.get(ORDERS_URL);
}

export function getOrderById(orderId) {
    return axios.get(`${ORDERS_URL}/${orderId}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findOrders(queryParams) {
    return axios.post(`${ORDERS_URL}/find`, { queryParams });
}

export function getCenters() {
    return axios.get(CENTERS_URL);
}

// UPDATE => PUT: update the procuct on the server
export function updateOrder(order) {
    return axios.put(`${ORDERS_URL}/${order._id}`, { order });
}

// UPDATE Status
export function updateStatusForOrders(ids, status) {
    return axios.post(`${ORDERS_URL}/updateStatusForOrders`, {
        ids,
        status
    });
}

// DELETE => delete the order from the server
export function deleteOrder(orderId) {
    return axios.delete(`${ORDERS_URL}/${orderId}`);
}

// DELETE Orders by ids
export function deleteOrders(ids) {
    return axios.post(`${ORDERS_URL}/deleteOrders`, { ids });
}
