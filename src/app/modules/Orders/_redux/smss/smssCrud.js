import axios from "axios";

export const SMSS_URL = process.env.REACT_APP_SERVER_URL + "/api/sms";

// CREATE =>  POST: add a new sms to the server
export function createSms(sms) {
  return axios.post(SMSS_URL, { sms });
}

// READ
// Server should return filtered smss by orderId
export function getAllOrderSmssByOrderId(orderId) {
  return axios.get(`${SMSS_URL}?orderId=${orderId}`);
}

export function getSmsById(smsId) {
  return axios.get(`${SMSS_URL}/${smsId}`);
}

// Server should return sorted/filtered smss and merge with items from state
// TODO: Change your URL to REAL API, right now URL is 'api/smssfind/{orderId}'. Should be 'api/smss/find/{orderId}'!!!
export function findSmss(queryParams, orderId) {
  return axios.post(`${SMSS_URL}/find/${orderId}`, { queryParams });
}

// UPDATE => PUT: update the sms
export function updateSms(sms) {
  return axios.put(`${SMSS_URL}/${sms.id}`, { sms });
}

// DELETE => delete the sms
export function deleteSms(smsId) {
  return axios.delete(`${SMSS_URL}/${smsId}`);
}

// DELETE Smss by ids
export function deleteSmss(ids) {
  return axios.post(`${SMSS_URL}/deleteSmss`, { ids });
}
