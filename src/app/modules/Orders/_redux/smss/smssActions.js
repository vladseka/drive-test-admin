import * as requestFromServer from "./smssCrud";
import { smssSlice, callTypes } from "./smssSlice";

const { actions } = smssSlice;

export const fetchSmss = (queryParams, orderId) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  if (!orderId) {
    return dispatch(actions.smssFetched({ totalCount: 0, entities: null }));
  }

  return requestFromServer
    .findSmss(queryParams, orderId)
    .then(response => {
      const { totalCount, entities } = response.data;
      dispatch(actions.smssFetched({ totalCount, entities }));
    })
    .catch(error => {
      error.clientMessage = "Can't find smss";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchSms = id => dispatch => {
  if (!id) {
    return dispatch(actions.smsFetched({ smsForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getSmsById(id)
    .then(response => {
      const sms = response.data;
      dispatch(actions.smsFetched({ smsForEdit: sms }));
    })
    .catch(error => {
      error.clientMessage = "Can't find sms";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteSms = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteSms(id)
    .then(response => {
      dispatch(actions.smsDeleted({ id }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete sms";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const createSms = smsForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createSms(smsForCreation)
    .then(response => {
      const { sms } = response.data;
      dispatch(actions.smsCreated({ sms }));
    })
    .catch(error => {
      error.clientMessage = "Can't create sms";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateSms = sms => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateSms(sms)
    .then(() => {
      dispatch(actions.smsUpdated({ sms }));
    })
    .catch(error => {
      error.clientMessage = "Can't update sms";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteSmss = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteSmss(ids)
    .then(() => {
      console.log("delete return");
      dispatch(actions.smssDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete smss";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
