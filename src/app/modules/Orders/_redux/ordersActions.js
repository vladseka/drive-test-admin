import * as requestFromServer from "./ordersCrud";
import { ordersSlice, callTypes } from "./ordersSlice";

const { actions } = ordersSlice;

export const fetchOrders = queryParams => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.list }));
    return requestFromServer
        .findOrders(queryParams)
        .then(response => {
            const { totalCount, entities } = response.data;
            dispatch(actions.ordersFetched({ totalCount, entities }));
        })
        .catch(error => {
            error.clientMessage = "Can't find orders";
            dispatch(actions.catchError({ error, callType: callTypes.list }));
        });
};

export const fetchCenters = () => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.list }));
    return requestFromServer
        .getCenters()
        .then(response => {
            const entities = response.data;
            dispatch(actions.centersFetched({ entities }));
        })
        .catch(error => {
            error.clientMessage = "Can't find centers";
            dispatch(actions.catchError({ error, callType: callTypes.list }));
        });
};

export const fetchOrder = id => dispatch => {
    if (!id) {
        return dispatch(actions.orderFetched({ orderForEdit: undefined }));
    }

    dispatch(actions.startCall({ callType: callTypes.action }));
    return requestFromServer
        .getOrderById(id)
        .then(response => {
            const order = response.data;
            order.id = order._id;
            dispatch(actions.orderFetched({ orderForEdit: order }));
        })
        .catch(error => {
            error.clientMessage = "Can't find order";
            dispatch(actions.catchError({ error, callType: callTypes.action }));
        });
};

export const deleteOrder = id => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.action }));
    return requestFromServer
        .deleteOrder(id)
        .then(() => {
            dispatch(actions.orderDeleted({ id }));
        })
        .catch(error => {
            error.clientMessage = "Can't delete order";
            dispatch(actions.catchError({ error, callType: callTypes.action }));
        });
};

export const createOrder = orderForCreation => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.action }));
    return requestFromServer
        .createOrder(orderForCreation)
        .then(response => {
            const { order } = response.data;
            dispatch(actions.orderCreated({ order }));
        })
        .catch(error => {
            error.clientMessage = "Can't create order";
            dispatch(actions.catchError({ error, callType: callTypes.action }));
        });
};

export const updateOrder = order => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.action }));
    return requestFromServer
        .updateOrder(order)
        .then(() => {
            dispatch(actions.orderUpdated({ order }));
        })
        .catch(error => {
            error.clientMessage = "Can't update order";
            dispatch(actions.catchError({ error, callType: callTypes.action }));
        });
};

export const updateOrdersStatus = (ids, status) => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.action }));
    return requestFromServer
        .updateStatusForOrders(ids, status)
        .then(() => {
            dispatch(actions.ordersStatusUpdated({ ids, status }));
        })
        .catch(error => {
            error.clientMessage = "Can't update orders status";
            dispatch(actions.catchError({ error, callType: callTypes.action }));
        });
};

export const deleteOrders = ids => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.action }));
    return requestFromServer
        .deleteOrders(ids)
        .then(() => {
            dispatch(actions.ordersDeleted({ ids }));
        })
        .catch(error => {
            error.clientMessage = "Can't delete orders";
            dispatch(actions.catchError({ error, callType: callTypes.action }));
        });
};
