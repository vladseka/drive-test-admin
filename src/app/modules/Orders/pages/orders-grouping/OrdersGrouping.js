import React, { useMemo } from "react";
import { useOrdersUIContext } from "../OrdersUIContext";

export function OrdersGrouping() {
  // Orders UI Context
  const ordersUIContext = useOrdersUIContext();
  const ordersUIProps = useMemo(() => {
    return {
      ids: ordersUIContext.ids,
      setIds: ordersUIContext.setIds,
      openDeleteOrdersDialog: ordersUIContext.openDeleteOrdersDialog,
      openFetchOrdersDialog: ordersUIContext.openFetchOrdersDialog,
      openUpdateOrdersStatusDialog:
        ordersUIContext.openUpdateOrdersStatusDialog,
    };
  }, [ordersUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{ordersUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              {/* <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={ordersUIProps.openDeleteOrdersDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              &nbsp; */}
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={ordersUIProps.openUpdateOrdersStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
