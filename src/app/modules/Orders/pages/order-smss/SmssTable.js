// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import * as actions from "../../_redux/smss/smssActions";
import * as uiHelpers from "./SmssUIHelper";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import {
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  // sortCaret,
} from "../../../../../_metronic/_helpers";
import { useSmssUIContext } from "./SmssUIContext";
import * as columnFormatters from "./column-formatters";

export function SmssTable() {
  // Smss UI Context
  const smssUIContext = useSmssUIContext();
  const smssUIProps = useMemo(() => {
    return {
      ids: smssUIContext.ids,
      setIds: smssUIContext.setIds,
      queryParams: smssUIContext.queryParams,
      setQueryParams: smssUIContext.setQueryParams,
      orderId: smssUIContext.orderId
    };
  }, [smssUIContext]);

  // Getting curret state of orders list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.smss }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  const dispatch = useDispatch();
  useEffect(() => {
    smssUIProps.setIds([]);
    dispatch(
      actions.fetchSmss(smssUIProps.queryParams, smssUIProps.orderId)
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [smssUIProps.queryParams, dispatch, smssUIProps.orderId]);
  const columns = [
    {
      dataField: "date",
      text: "Date",
      formatter: columnFormatters.DateColumnFormatter,
    },
    {
      dataField: "time",
      text: "Time",
      formatter: columnFormatters.TimeColumnFormatter,
    },
    {
      dataField: "type",
      text: "Type",
      formatter: columnFormatters.TypeColumnFormatter,
    },
    {
      dataField: "message",
      text: "Message",
    },
  ];

  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: smssUIProps.queryParams.pageSize,
    page: smssUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center"
                bordered={false}
                bootstrap4
                remote
                keyField="id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  smssUIProps.setQueryParams
                )}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
