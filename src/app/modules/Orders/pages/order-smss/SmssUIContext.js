/* eslint-disable no-unused-vars */
import React, { useEffect, useContext, createContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./SmssUIHelper";

const SmssUIContext = createContext();

export function useSmssUIContext() {
  return useContext(SmssUIContext);
}

export const SmssUIConsumer = SmssUIContext.Consumer;

export function SmssUIProvider({ currentOrderId, children }) {
  const [orderId, setOrderId] = useState(currentOrderId);
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback(nextQueryParams => {
    setQueryParamsBase(prevQueryParams => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);
  const initSms = {
    id: undefined,
    date: "",
    time: "",
    type: 0,
    message: ""
  };
  useEffect(() => {
    initSms.orderId = currentOrderId;
    initSms.carId = currentOrderId;
    setOrderId(currentOrderId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentOrderId]);

  const value = {
    ids,
    setIds,
    orderId,
    setOrderId,
    queryParams,
    setQueryParams,
    initSms
  };

  return (
    <SmssUIContext.Provider value={value}>
      {children}
    </SmssUIContext.Provider>
  );
}
