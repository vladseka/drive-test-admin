import React from "react";

export const TimeColumnFormatter = (cellContent, row) => {
    let time = new Date(row.date);
    time = time.getHours() +
        ":" + time.getMinutes() +
        ":" + time.getSeconds();
    return (<>{time}</>)
}
