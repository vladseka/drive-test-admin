import React from "react";
import {
  SmsTypeTitles
} from "../SmssUIHelper";

export const TypeColumnFormatter = (cellContent, row) => (
  <>
    {SmsTypeTitles[row.type]}
  </>
);
