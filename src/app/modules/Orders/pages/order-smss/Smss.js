import React from "react";
// import { SmssFilter } from "./SmssFilter";
import { SmssTable } from "./SmssTable";
import { SmssLoadingDialog } from "./SmssLoadingDialog";

export function Smss() {
  return (
    <>
      <SmssLoadingDialog />
      <div className="form margin-b-30">
        {/* <SmssFilter /> */}
      </div>
      <SmssTable />
    </>
  );
}
