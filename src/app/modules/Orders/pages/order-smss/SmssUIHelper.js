export const defaultSorted = [{ dataField: "id", order: "desc" }];
export const SmsTypeTitles = ["Forward", "Reply"];
export const sizePerPageList = [
  { text: "1", value: 1 },
  { text: "3", value: 3 },
  { text: "5", value: 5 }
];
export const initialFilter = {
  sortOrder: "desc", // asc||desc
  sortField: "id",
  pageNumber: 1,
  pageSize: 5
};
