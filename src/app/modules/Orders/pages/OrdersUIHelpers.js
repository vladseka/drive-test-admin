export const OrderStatusCssClasses = ["success", "info", "danger", ""];
export const OrderStatusTitles = ["Pending", "Approved", "Completed"];
export const OrderPackageCssClasses = ["success", "danger", ""];
export const OrderPackageTitles = ["One Time", "Life Time"];
export const OrderLicenceTitles = ["Driving test reference number", "Theory test number"];
export const OrderRefundTitles = ["No", "Refund"];
export const OrderPaidTitles = ["Not yet", "Made Payment"];
export const OrderTimeTitles = ["0 AM", "1 AM", "2 AM", "3 AM", "4 AM", "5 AM", "6 AM", "7 AM", "8 AM", "9 AM", "10 AM", "11 AM", "12 PM", "1 PM", "2 PM", "3 PM", "4 PM", "5 PM", "6 PM", "7 PM", "8 PM", "9 PM", "10 PM", "11 PM"];
export const defaultSorted = [{ dataField: "id", order: "asc" }];
export const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
];
export const initialFilter = {
    filter: {
        name: "",
        phone: "",
        refund: false,
        paid: true
    },
    sortOrder: "asc", // asc||desc
    sortField: "id",
    pageNumber: 1,
    pageSize: 10
};