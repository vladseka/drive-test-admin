/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../_redux/ordersActions";
import { useOrdersUIContext } from "../OrdersUIContext";

export function OrdersDeleteDialog({ show, onHide }) {
  // Orders UI Context
  const ordersUIContext = useOrdersUIContext();
  const ordersUIProps = useMemo(() => {
    return {
      ids: ordersUIContext.ids,
      setIds: ordersUIContext.setIds,
      queryParams: ordersUIContext.queryParams,
    };
  }, [ordersUIContext]);

  // Orders Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.orders.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => { }, [isLoading, dispatch]);

  // if there weren't selected orders we should close modal
  useEffect(() => {
    if (!ordersUIProps.ids || ordersUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ordersUIProps.ids]);

  const deleteOrders = () => {
    // server request for deleting order by seleted ids
    dispatch(actions.deleteOrders(ordersUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchOrders(ordersUIProps.queryParams)).then(() => {
        // clear selections list
        ordersUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Orders Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected orders?</span>
        )}
        {isLoading && <span>Orders are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteOrders}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
