import React, { useMemo } from "react";
import {
    Card,
    CardBody,
    CardHeader,
    CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { OrdersFilter } from "./orders-filter/OrdersFilter";
import { OrdersTable } from "./orders-table/OrdersTable";
import { OrdersGrouping } from "./orders-grouping/OrdersGrouping";
import { useOrdersUIContext } from "./OrdersUIContext";

export function OrdersCard({ status }) {
    const ordersUIContext = useOrdersUIContext();
    const ordersUIProps = useMemo(() => {
        return {
            ids: ordersUIContext.ids,
            queryParams: ordersUIContext.queryParams,
            setQueryParams: ordersUIContext.setQueryParams,
            newOrderButtonClick: ordersUIContext.newOrderButtonClick,
            openDeleteOrdersDialog: ordersUIContext.openDeleteOrdersDialog,
            openEditOrderPage: ordersUIContext.openEditOrderPage,
            openUpdateOrdersStatusDialog:
                ordersUIContext.openUpdateOrdersStatusDialog,
            openFetchOrdersDialog: ordersUIContext.openFetchOrdersDialog,
        };
    }, [ordersUIContext]);

    return (
        <Card>
            <CardHeader title="Orders List">
                <CardHeaderToolbar>
                    {/* <button
                        type="button"
                        className="btn btn-primary"
                        onClick={ordersUIProps.newOrderButtonClick}
                    >
                        New Order
          </button> */}
                </CardHeaderToolbar>
            </CardHeader>
            <CardBody>
                <OrdersFilter status={status} />
                {ordersUIProps.ids.length > 0 && (
                    <>
                        <OrdersGrouping />
                    </>
                )}
                <OrdersTable status={status} />
            </CardBody>
        </Card>
    );
}
