import React, { useEffect, useState } from "react";
import { Route } from "react-router-dom";
import { OrdersLoadingDialog } from "./orders-loading-dialog/OrdersLoadingDialog";
import { OrderDeleteDialog } from "./order-delete-dialog/OrderDeleteDialog";
import { OrdersDeleteDialog } from "./orders-delete-dialog/OrdersDeleteDialog";
import { OrderEdit } from "./order-edit/OrderEdit";
import { OrdersUpdateStatusDialog } from "./orders-update-status-dialog/OrdersUpdateStatusDialog";
import { OrdersCard } from "./OrdersCard";
import { OrdersUIProvider } from "./OrdersUIContext";

export function OrdersPage({ history, match }) {
    const [status, setStatus] = useState(match.params.status)
    useEffect(() => {
        if (match.params.status) {
            setStatus(match.params.status)
        }
    }, [match.params.status])
    const ordersUIEvents = {
        newOrderButtonClick: () => {
            history.push("/orders/new");
        },
        openEditOrderPage: (id) => {
            history.push(`/orders/${id}/edit`);
        },
        openDeleteOrderDialog: (id) => {
            history.push(`/orders/${id}/delete`);
        },
        openDeleteOrdersDialog: () => {
            history.push(`/orders/deleteOrders`);
        },
        openFetchOrdersDialog: () => {
            history.push(`/orders/fetch`);
        },
        openUpdateOrdersStatusDialog: () => {
            history.push("/orders/updateStatus");
        },
    };

    return (
        <OrdersUIProvider ordersUIEvents={ordersUIEvents}>
            <OrdersLoadingDialog />
            <Route path="/orders/deleteOrders">
                {({ history, match }) => (
                    <OrdersDeleteDialog
                        show={match != null}
                        onHide={() => {
                            history.push("/orders");
                        }}
                    />
                )}
            </Route>
            <Route path="/orders/:id/delete">
                {({ history, match }) => (
                    <OrderDeleteDialog
                        show={match != null}
                        id={match && match.params.id}
                        onHide={() => {
                            history.push("/orders");
                        }}
                    />
                )}
            </Route>
            <Route path="/orders/updateStatus">
                {({ history, match }) => (
                    <OrdersUpdateStatusDialog
                        show={match != null}
                        onHide={() => {
                            history.push("/orders");
                        }}
                    />
                )}
            </Route>
            <Route path="/orders/:id/edit" component={OrderEdit} />
            <OrdersCard status={status} />
        </OrdersUIProvider>
    );
}
