import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { OrderStatusCssClasses } from "../OrdersUIHelpers";
import { useOrdersUIContext } from "../OrdersUIContext";

const selectedOrders = (entities, ids) => {
  const _orders = [];
  ids.forEach((id) => {
    const order = entities.find((el) => el.id === id);
    if (order) {
      _orders.push(order);
    }
  });
  return _orders;
};

export function OrdersFetchDialog({ show, onHide }) {
  // Orders UI Context
  const ordersUIContext = useOrdersUIContext();
  const ordersUIProps = useMemo(() => {
    return {
      ids: ordersUIContext.ids,
      queryParams: ordersUIContext.queryParams,
    };
  }, [ordersUIContext]);

  // Orders Redux state
  const { orders } = useSelector(
    (state) => ({
      orders: selectedOrders(state.orders.entities, ordersUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!ordersUIProps.ids || ordersUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ordersUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {orders.map((order) => (
              <div className="list-timeline-item mb-3" key={order.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      OrderStatusCssClasses[order.status]
                      } label-inline`}
                    style={{ width: "60px" }}
                  >
                    {order.name}
                  </span>{" "}
                  <span className="ml-5">
                    {order.date.substring(0, 10)}, {order.phone}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
