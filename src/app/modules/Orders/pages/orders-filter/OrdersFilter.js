import React, { useMemo } from "react";
import { Formik } from "formik";
import { isEqual } from "lodash";
import { useOrdersUIContext } from "../OrdersUIContext";

const prepareFilter = (queryParams, values) => {
  const { status, type, searchText } = values;
  const newQueryParams = { ...queryParams };
  const filter = {};
  // Filter by status
  filter.status = status !== "" ? +status : undefined;
  // Filter by package
  filter.type = type !== "" ? + type : undefined;
  // Filter by all fields
  filter.id = searchText;
  filter.refund = 0;
  filter.paid = 1;
  if (searchText) {
    filter.name = searchText;
    filter.phone = searchText;
  }
  newQueryParams.filter = filter;
  return newQueryParams;
};

export function OrdersFilter({ listLoading }) {
  // Orders UI Context
  const ordersUIContext = useOrdersUIContext();
  const ordersUIProps = useMemo(() => {
    return {
      setQueryParams: ordersUIContext.setQueryParams,
      queryParams: ordersUIContext.queryParams,
    };
  }, [ordersUIContext]);

  const applyFilter = (values) => {
    const newQueryParams = prepareFilter(ordersUIProps.queryParams, values);
    if (!isEqual(newQueryParams, ordersUIProps.queryParams)) {
      newQueryParams.pageNumber = 1;
      ordersUIProps.setQueryParams(newQueryParams);
    }
  };

  return (
    <>
      <Formik
        initialValues={{
          status: "", // values => All=""/Selling=0/Sold=1
          type: "", // values => All=""/New=0/Used=1
          refund: 0,
          paid: 1,
          searchText: "",
        }}
        onSubmit={(values) => {
          applyFilter(values);
        }}
      >
        {({
          values,
          handleSubmit,
          handleBlur,
          handleChange,
          setFieldValue,
        }) => (
            <form onSubmit={handleSubmit} className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-2">
                  <select
                    className="form-control"
                    name="status"
                    placeholder="Filter by Status"
                    onChange={(e) => {
                      setFieldValue("status", e.target.value);
                      handleSubmit();
                    }}
                    onBlur={handleBlur}
                    value={values.status}
                  >
                    <option value="">All</option>
                    <option value="0">Pending</option>
                    <option value="1">Approved</option>
                    <option value="2">Completed</option>
                  </select>
                  <small className="form-text text-muted">
                    <b>Filter</b> by Status
                </small>
                </div>
                <div className="col-lg-2">
                  <select
                    className="form-control"
                    placeholder="Filter by Package"
                    name="type"
                    onBlur={handleBlur}
                    onChange={(e) => {
                      setFieldValue("type", e.target.value);
                      handleSubmit();
                    }}
                    value={values.condition}
                  >
                    <option value="">All</option>
                    <option value="0">One Time</option>
                    <option value="1">Life Time</option>
                  </select>
                  <small className="form-text text-muted">
                    <b>Filter</b> by Package
                </small>
                </div>
                <div className="col-lg-2">
                  <input
                    type="text"
                    className="form-control"
                    name="searchText"
                    placeholder="Search"
                    onBlur={handleBlur}
                    value={values.searchText}
                    onChange={(e) => {
                      setFieldValue("searchText", e.target.value);
                      handleSubmit();
                    }}
                  />
                  <small className="form-text text-muted">
                    <b>Search</b> in all fields
                </small>
                </div>
              </div>
            </form>
          )}
      </Formik>
    </>
  );
}
