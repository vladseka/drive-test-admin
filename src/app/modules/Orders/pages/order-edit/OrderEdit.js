/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../_redux/ordersActions";
import {
    Card,
    CardBody,
    CardHeader,
    CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { OrderEditForm } from "./OrderEditForm";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { SmssUIProvider } from "../order-smss/SmssUIContext";
import { Smss } from "../order-smss/Smss";
import { useOrdersUIContext } from "../OrdersUIContext";

const initOrder = {
    id: undefined,
    afterDate: "",
    beforeDate: "",
    afterTime: 0,
    beforeTime: 23,
    numberType: 0,
    licence: "",
    testNumber: "",
    center: "",
    name: "",
    addReq: "",
    comment: "",
    refund: 0,
    paid: 0,
    refundAmount: 0,
    email: "",
    phone: "",
    type: 0,
    status: 0
};

export function OrderEdit({
    history,
    match: {
        params: { id },
    },
}) {
    // Subheader
    const suhbeader = useSubheader();

    const OrdersUIContext = useOrdersUIContext();

    // Tabs
    const [tab, setTab] = useState("basic");
    const [title, setTitle] = useState("");
    const dispatch = useDispatch();
    // const layoutDispatch = useContext(LayoutContext.Dispatch);
    const { actionsLoading, orderForEdit, centersList } = useSelector(
        (state) => ({
            actionsLoading: state.orders.actionsLoading,
            orderForEdit: state.orders.orderForEdit,
            centersList: state.orders.centers
        }),
        shallowEqual
    );

    useEffect(() => {
        dispatch(actions.fetchOrder(id));
    }, [id, dispatch]);

    useEffect(() => {
        let _title = id ? "" : "New Order";
        if (orderForEdit && id) {
            _title = `Edit order '${orderForEdit.name} - ${orderForEdit.phone}'`;
        }

        setTitle(_title);
        suhbeader.setTitle(_title);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [orderForEdit, id]);

    const saveOrder = (values) => {
        if (!id) {
            dispatch(actions.createOrder(values)).then(() => backToOrdersList());
        } else {
            dispatch(actions.updateOrder(values)).then(() => {
                if (values.refund === '1' || values.paid === '0') {
                    console.log(values)
                    dispatch(actions.fetchOrders(OrdersUIContext.queryParams))
                    backToOrdersList()
                } else {
                    backToOrdersList()
                }
            });
        }
    };

    const btnRef = useRef();
    const saveOrderClick = () => {
        if (btnRef && btnRef.current) {
            btnRef.current.click();
        }
    };

    const backToOrdersList = () => {
        history.push(`/orders`);
    };

    return (
        <Card>
            {actionsLoading && <ModalProgressBar />}
            <CardHeader title={title}>
                <CardHeaderToolbar>
                    <button
                        type="button"
                        onClick={backToOrdersList}
                        className="btn btn-light"
                    >
                        <i className="fa fa-arrow-left"></i>
            Back
          </button>
                    {`  `}
                    <button
                        type="submit"
                        className="btn btn-primary ml-2"
                        onClick={saveOrderClick}
                    >
                        Save
          </button>
                </CardHeaderToolbar>
            </CardHeader>
            <CardBody>
                <ul className="nav nav-tabs nav-tabs-line " role="tablist">
                    <li className="nav-item" onClick={() => setTab("basic")}>
                        <a
                            className={`nav-link ${tab === "basic" && "active"}`}
                            data-toggle="tab"
                            role="tab"
                            aria-selected={(tab === "basic").toString()}
                        >
                            Basic info
            </a>
                    </li>
                    {id && (
                        <>
                            {" "}
                            <li className="nav-item" onClick={() => setTab("smss")}>
                                <a
                                    className={`nav-link ${tab === "smss" && "active"}`}
                                    data-toggle="tab"
                                    role="button"
                                    aria-selected={(tab === "smss").toString()}
                                >
                                    SMS History
                </a>
                            </li>
                        </>
                    )}
                </ul>
                <div className="mt-5">
                    {tab === "basic" && (
                        <OrderEditForm
                            actionsLoading={actionsLoading}
                            order={orderForEdit || initOrder}
                            centersArr={centersList}
                            btnRef={btnRef}
                            saveOrder={saveOrder}
                        />
                    )}
                    {tab === "smss" && id && (
                        <SmssUIProvider currentOrderId={id}>
                            <Smss />
                        </SmssUIProvider>
                    )}
                </div>
            </CardBody>
        </Card>
    );
}
