// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React, { useEffect, useState } from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input, Select, DatePickerField } from "../../../../../_metronic/_partials/controls";
import {
  OrderStatusTitles,
  OrderPackageTitles,
  OrderTimeTitles,
  OrderLicenceTitles,
  OrderRefundTitles,
  OrderPaidTitles
} from "../OrdersUIHelpers";

// Validation schema
const OrderEditSchema = Yup.object().shape({
  date: Yup.string()
    .required("Date is required"),
  name: Yup.string()
    .min(2, "Minimum 2 symbols")
    .max(50, "Maximum 30 symbols")
    .required("Name is required"),
  phone: Yup.string()
    .min(7, "Minimum 7 symbols")
    .max(16, "Maximum 16 symbols")
    .required("Mobile number is required"),
  refundAmount: Yup.number()
    .required("Refund amount is required"),
});

export function OrderEditForm({ order, centersArr, btnRef, saveOrder }) {

  const [centers, setCenters] = useState([]);
  useEffect(() => {
    setCenters(centersArr)
  }, []);

  const [refund123, setRefund] = useState(false);
  const toggleRefund = (value) => {
    setRefund((value == 1) ? true : false);
  }
  console.log(refund123)


  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={order}
        validationSchema={OrderEditSchema}
        onSubmit={(values) => {
          saveOrder(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4 mb-4">
                  <Field
                    name="name"
                    component={Input}
                    placeholder="Name"
                    label="Name"
                  />
                </div>
                <div className="col-lg-4 mb-4">
                  <Field
                    name="email"
                    component={Input}
                    placeholder="Email"
                    label="Email"
                  />
                </div>
                <div className="col-lg-4 mb-4">
                  <Field
                    name="phone"
                    component={Input}
                    placeholder="Phone"
                    label="Phone"
                  />
                </div>
                <div className="col-lg-4 mb-4">
                  <Select name="center" label="Desired Center">
                    {centers.map((center, index) => (
                      <option key={center.name} value={center._id}>
                        {center.name}
                      </option>
                    ))}
                  </Select>
                </div>
                <div className="col-lg-4 mb-4">
                  <DatePickerField
                    name="afterDate"
                    label="Test After"
                  />
                </div>
                <div className="col-lg-4 mb-4">
                  <DatePickerField
                    name="beforeDate"
                    label="Test Before"
                  />
                </div>
                <div className="col-lg-4 mb-4">
                  <Select name="afterTime" label="Earliest Time">
                    {OrderTimeTitles.map((time, index) => (
                      <option key={time} value={index}>
                        {time}
                      </option>
                    ))}
                  </Select>
                </div>
                <div className="col-lg-4 mb-4">
                  <Select name="beforeTime" label="Latest Time">
                    {OrderTimeTitles.map((time, index) => (
                      <option key={time} value={index}>
                        {time}
                      </option>
                    ))}
                  </Select>
                </div>
                <div className="col-lg-4 mb-4">
                  <Field
                    name="licence"
                    component={Input}
                    placeholder="Licence number"
                    label="Licence Number"
                  />
                </div>
                <div className="col-lg-4 mb-4">
                  <Select name="numberType" label="Test Number Type">
                    {OrderLicenceTitles.map((licence, index) => (
                      <option key={licence} value={index}>
                        {licence}
                      </option>
                    ))}
                  </Select>
                </div>
                <div className="col-lg-4 mb-4">
                  <Field
                    name="testNumber"
                    component={Input}
                    placeholder="Test number"
                    label="Test Number"
                  />
                </div>
                <div className="col-lg-4 mb-4">
                  <Select name="status" label="Status">
                    {OrderStatusTitles.map((status, index) => (
                      <option key={status} value={index}>
                        {status}
                      </option>
                    ))}
                  </Select>
                </div>
                <div className="col-lg-4 mb-4">
                  <Select name="type" label="Package">
                    {OrderPackageTitles.map((type, index) => (
                      <option key={type} value={index}>
                        {type}
                      </option>
                    ))}
                  </Select>
                </div>
                <div className="col-lg-4 mb-4">
                  <Select name="refund" label="Refund" handleChange={toggleRefund}>
                    {OrderRefundTitles.map((refund, index) => (
                      <option key={refund} value={index}>
                        {refund}
                      </option>
                    ))}
                  </Select>
                </div>
                <div className="col-lg-4 mb-4">
                  <Field
                    disabled={!refund123}
                    name="refundAmount"
                    component={Input}
                    placeholder="Refund amount"
                    label="Refund amount"
                  />
                </div>
                <div className="col-lg-4 mb-4">
                  <Select name="paid" label="Made Payment">
                    {OrderPaidTitles.map((paid, index) => (
                      <option key={paid} value={index}>
                        {paid}
                      </option>
                    ))}
                  </Select>
                </div>
                <div className="col-lg-4 mb-4">
                  <label>Additional requirements</label>
                  <Field
                    name="addReq"
                    as="textarea"
                    rows="4"
                    className="form-control"
                  />
                </div>
                <div className="col-lg-4 mb-4">
                  <label>Comment</label>
                  <Field
                    name="comment"
                    as="textarea"
                    rows="4"
                    className="form-control"
                  />
                </div>
              </div>
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
