import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { OrderStatusCssClasses } from "../OrdersUIHelpers";
import * as actions from "../../_redux/ordersActions";
import { useOrdersUIContext } from "../OrdersUIContext";

const selectedOrders = (entities, ids) => {
  const _orders = [];
  ids.forEach((id) => {
    const order = entities.find((el) => el.id === id);
    if (order) {
      _orders.push(order);
    }
  });
  return _orders;
};

export function OrdersUpdateStatusDialog({ show, onHide }) {
  // Orders UI Context
  const ordersUIContext = useOrdersUIContext();
  const ordersUIProps = useMemo(() => {
    return {
      ids: ordersUIContext.ids,
      setIds: ordersUIContext.setIds,
      queryParams: ordersUIContext.queryParams,
    };
  }, [ordersUIContext]);

  // Orders Redux state
  const { orders, isLoading } = useSelector(
    (state) => ({
      orders: selectedOrders(state.orders.entities, ordersUIProps.ids),
      isLoading: state.orders.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected orders we should close modal
  useEffect(() => {
    if (ordersUIProps.ids || ordersUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ordersUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing order by ids
    dispatch(actions.updateOrdersStatus(ordersUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchOrders(ordersUIProps.queryParams)).then(
          () => {
            // clear selections list
            ordersUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected orders
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {orders.map((order) => (
              <div className="list-timeline-item mb-3" key={order.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      OrderStatusCssClasses[order.status]
                      } label-inline`}
                    style={{ width: "60px" }}
                  >
                    {order.name}
                  </span>{" "}
                  <span className="ml-5">
                    {order.phone}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${OrderStatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Pending</option>
            <option value="1">Approved</option>
            <option value="2">Completed</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
