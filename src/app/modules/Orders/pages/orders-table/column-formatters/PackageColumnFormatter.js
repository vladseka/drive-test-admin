import React from "react";
import {
  OrderPackageCssClasses,
  OrderPackageTitles
} from "../../OrdersUIHelpers";

export const PackageColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        OrderPackageCssClasses[row.type]
        } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        OrderPackageCssClasses[row.type]
        }`}
    >
      {OrderPackageTitles[row.type]}
    </span>
  </>
);
