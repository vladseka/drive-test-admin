import React from "react";

export const DateColumnFormatter = (cellContent, row) => {
    let date = new Date(row.date);
    date = (date.getMonth() + 1) +
        "/" + date.getDate() +
        "/" + date.getFullYear();
    return (<>{date}</>)
}
