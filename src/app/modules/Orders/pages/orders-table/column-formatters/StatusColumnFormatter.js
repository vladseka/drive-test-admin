import React from "react";
import {
  OrderStatusCssClasses,
  OrderStatusTitles
} from "../../OrdersUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      OrderStatusCssClasses[row.status]
      } label-inline`}
  >
    {OrderStatusTitles[row.status]}
  </span>
);
