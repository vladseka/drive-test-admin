// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
    PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../_redux/ordersActions";
import * as uiHelpers from "../OrdersUIHelpers";
import {
    getSelectRow,
    getHandlerTableChange,
    NoRecordsFoundMessage,
    PleaseWaitMessage,
    sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useOrdersUIContext } from "../OrdersUIContext";

export function OrdersTable({ status }) {
    // Orders UI Context
    const ordersUIContext = useOrdersUIContext();
    const ordersUIProps = useMemo(() => {
        return {
            ids: ordersUIContext.ids,
            setIds: ordersUIContext.setIds,
            queryParams: ordersUIContext.queryParams,
            setQueryParams: ordersUIContext.setQueryParams,
            openEditOrderPage: ordersUIContext.openEditOrderPage,
            openDeleteOrderDialog: ordersUIContext.openDeleteOrderDialog,
        };
    }, [ordersUIContext]);

    useEffect(() => {
        if (status && status < 3) {
            ordersUIProps.queryParams.filter.status = status;
        } else {
            ordersUIProps.queryParams.filter.status = undefined;
        }
        console.log(status)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [status])

    // Getting curret state of orders list from store (Redux)
    const { currentState } = useSelector(
        (state) => ({ currentState: state.orders }),
        shallowEqual
    );
    const { totalCount, entities, listLoading } = currentState;
    // Orders Redux state
    const dispatch = useDispatch();
    useEffect(() => {
        // clear selections list
        ordersUIProps.setIds([]);
        // server call by queryParams
        dispatch(actions.fetchOrders(ordersUIProps.queryParams));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [ordersUIProps.queryParams, dispatch]);
    useEffect(() => {
        dispatch(actions.fetchCenters());
    }, [])
    // Table columns
    const columns = [
        {
            dataField: "testNumber",
            text: "Order ID",
            formatter: columnFormatters.IdColumnFormatter,
        },
        {
            dataField: "name",
            text: "Name",
            formatter: columnFormatters.NameColumnFormatter,
        },
        {
            dataField: "phone",
            text: "Mobile No.",
        },
        {
            dataField: "type",
            text: "Package",
            sort: true,
            sortCaret: sortCaret,
            formatter: columnFormatters.PackageColumnFormatter,
        },
        {
            dataField: "status",
            text: "Status",
            sort: true,
            sortCaret: sortCaret,
            formatter: columnFormatters.StatusColumnFormatter,
        },
        {
            dataField: "action",
            text: "Actions",
            formatter: columnFormatters.ActionsColumnFormatter,
            formatExtraData: {
                openEditOrderPage: ordersUIProps.openEditOrderPage,
                // openDeleteOrderDialog: ordersUIProps.openDeleteOrderDialog,
            },
            classes: "text-right pr-0",
            headerClasses: "text-right pr-3",
            style: {
                minWidth: "100px",
            },
        },
    ];
    // Table pagination properties
    const paginationOptions = {
        custom: true,
        totalSize: totalCount,
        sizePerPageList: uiHelpers.sizePerPageList,
        sizePerPage: ordersUIProps.queryParams.pageSize,
        page: ordersUIProps.queryParams.pageNumber,
    };
    return (
        <>
            <PaginationProvider pagination={paginationFactory(paginationOptions)}>
                {({ paginationProps, paginationTableProps }) => {
                    return (
                        <Pagination
                            isLoading={listLoading}
                            paginationProps={paginationProps}
                        >
                            <BootstrapTable
                                wrapperClasses="table-responsive"
                                classes="table table-head-custom table-vertical-center"
                                bootstrap4
                                bordered={false}
                                remote
                                keyField="testNumber"
                                data={entities === null ? [] : entities}
                                columns={columns}
                                defaultSorted={uiHelpers.defaultSorted}
                                onTableChange={getHandlerTableChange(
                                    ordersUIProps.setQueryParams
                                )}
                                selectRow={getSelectRow({
                                    entities,
                                    ids: ordersUIProps.ids,
                                    setIds: ordersUIProps.setIds,
                                })}
                                {...paginationTableProps}
                            >
                                <PleaseWaitMessage entities={entities} />
                                <NoRecordsFoundMessage entities={entities} />
                            </BootstrapTable>
                        </Pagination>
                    );
                }}
            </PaginationProvider>
        </>
    );
}
