import React, { useMemo, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux"
import objectPath from "object-path";
import ApexCharts from "apexcharts";
import { useHtmlClassService } from "../../../../_metronic/layout";
import { actions as statsActions } from "./_redux/statsRedux"
import { getCountApprovedOrder } from "./_redux/statsCrud"
import { Link } from "react-router-dom"

export function StatsWidgetOrderApproved({ className }) {
    const uiService = useHtmlClassService();

    const dispatch = useDispatch();

    const stats = useSelector(state => state.stats)

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const init = () => {
        getCountApprovedOrder()
            .then(({ data: { count } }) => {
                dispatch(statsActions.getApprovedOrders(count));
            })
    }

    const layoutProps = useMemo(() => {
        return {
            colorsGrayGray500: objectPath.get(
                uiService.config,
                "js.colors.gray.gray500"
            ),
            colorsGrayGray200: objectPath.get(
                uiService.config,
                "js.colors.gray.gray200"
            ),
            colorsGrayGray300: objectPath.get(
                uiService.config,
                "js.colors.gray.gray300"
            ),
            colorsThemeBaseInfo: objectPath.get(
                uiService.config,
                "js.colors.theme.base.info"
            ),
            colorsThemeLightInfo: objectPath.get(
                uiService.config,
                "js.colors.theme.light.info"
            ),
            fontFamily: objectPath.get(uiService.config, "js.fontFamily")
        };
    }, [uiService]);

    useEffect(() => {
        const element = document.getElementById("stats_widget_approved_order");
        if (!element) {
            return;
        }

        const options = getChartOption(layoutProps);
        const chart = new ApexCharts(element, options);
        chart.render();
        init()
        return function cleanUp() {
            chart.destroy();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [layoutProps]);

    return (
        <div className={`card card-custom ${className}`}>
            <div className="card-body d-flex flex-column p-0">
                <div className="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                    <div className="d-flex flex-column mr-2">
                        <Link to='/orders/1' className="text-info text-hover-info font-weight-bolder font-size-h5">Approved Orders</Link>
                        <span className="text-muted font-weight-bold mt-2">Your Approved Orders Chart</span>
                    </div>
                    <span className="symbol symbol-light-info symbol-45">
                        <span className="symbol-label font-weight-bolder font-size-h6">+{stats.approvedOrders}</span>
                    </span>
                </div>
                <div
                    id="stats_widget_approved_order"
                    className="card-rounded-bottom"
                    style={{ height: "150px" }}
                ></div>
            </div>
        </div>
    );
}

function getChartOption(layoutProps) {
    const options = {
        series: [
            {
                name: "Net Profit",
                data: [32, 72, 21, 50, 30]
            }
        ],
        chart: {
            type: "area",
            height: 150,
            toolbar: {
                show: false
            },
            zoom: {
                enabled: false
            },
            sparkline: {
                enabled: true
            }
        },
        plotOptions: {},
        legend: {
            show: false
        },
        dataLabels: {
            enabled: false
        },
        fill: {
            type: "solid",
            opacity: 1
        },
        stroke: {
            curve: "smooth",
            show: true,
            width: 3,
            colors: [layoutProps.colorsThemeBaseInfo]
        },
        xaxis: {
            categories: ["Feb", "Mar", "Apr", "May", "Jun", "Jul"],
            axisBorder: {
                show: false
            },
            axisTicks: {
                show: false
            },
            labels: {
                show: false,
                style: {
                    colors: layoutProps.colorsGrayGray500,
                    fontSize: "12px",
                    fontFamily: layoutProps.fontFamily
                }
            },
            crosshairs: {
                show: false,
                position: "front",
                stroke: {
                    color: layoutProps.colorsGrayGray300,
                    width: 1,
                    dashArray: 3
                }
            },
            tooltip: {
                enabled: false,
                formatter: undefined,
                offsetY: 0,
                style: {
                    fontSize: "12px",
                    fontFamily: layoutProps.fontFamily
                }
            }
        },
        yaxis: {
            labels: {
                show: false,
                style: {
                    colors: layoutProps.colorsGrayGray500,
                    fontSize: "12px",
                    fontFamily: layoutProps.fontFamily
                }
            }
        },
        states: {
            normal: {
                filter: {
                    type: "none",
                    value: 0
                }
            },
            hover: {
                filter: {
                    type: "none",
                    value: 0
                }
            },
            active: {
                allowMultipleDataPointsSelection: false,
                filter: {
                    type: "none",
                    value: 0
                }
            }
        },
        tooltip: {
            enabled: false,
            style: {
                fontSize: "12px",
                fontFamily: layoutProps.fontFamily
            },
            y: {
                formatter: function (val) {
                    return "$" + val + " thousands";
                }
            }
        },
        colors: [layoutProps.colorsThemeLightInfo],
        markers: {
            colors: [layoutProps.colorsThemeLightInfo],
            strokeColor: [layoutProps.colorsThemeBaseInfo],
            strokeWidth: 3
        }
    };
    return options;
}
