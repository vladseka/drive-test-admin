import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
// import { put, takeLatest } from "redux-saga/effects";
// import * as stats from "./statsCrud";

export const actionTypes = {
  getTotalOrders: "[getTotalOrders] Action",
  getPendingOrders: "[getPendingOrders] Action",
  getApprovedOrders: "[getApprovedOrders] Action",
  getCompletedOrders: "[getCompletedOrders] Action",
  getRefundedOrders: "[getRefundedOrders] Action",
  getUnpaidOrders: "[getUnpaidOrders] Action",
  getTotalAffiliates: "[getTotalAffiliates] Action",
  getTotalUsers: "[getTotalUsers] Action",
  passOrderStatus: "[passOrderStatus] Action",
};

const initialState = {
  totalOrders: 0,
  pendingOrders: 0,
  approvedOrders: 0,
  completedOrders: 0,
  refundedOrders: 0,
  unpaidOrders: 0,
  totalAffiliates: 0,
  totalUsers: 0,
};

export const reducer = persistReducer(
  { storage, key: "stats-777", whitelist: ["totalOrders", "pendingOrders", "approvedOrders", "completedOrders", "totalAffiliates", "totalUsers"] },
  (state = initialState, action) => {
    switch (action.type) {
      case actionTypes.getTotalOrders: {
        const { count } = action.payload;
        return { ...state, totalOrders: count };
      }
      case actionTypes.getPendingOrders: {
        const { count } = action.payload;
        return { ...state, pendingOrders: count };
      }
      case actionTypes.getApprovedOrders: {
        const { count } = action.payload;
        return { ...state, approvedOrders: count };
      }
      case actionTypes.getCompletedOrders: {
        const { count } = action.payload;
        return { ...state, completedOrders: count };
      }
      case actionTypes.getRefundedOrders: {
        const { count } = action.payload;
        return { ...state, refundedOrders: count };
      }
      case actionTypes.getUnpaidOrders: {
        const { count } = action.payload;
        return { ...state, unpaidOrders: count };
      }
      case actionTypes.getTotalUsers: {
        const { count } = action.payload;
        return { ...state, totalUsers: count };
      }
      case actionTypes.getTotalAffiliates: {
        const { count } = action.payload;
        return { ...state, totalAffiliates: count };
      }
      default:
        return state;
    }
  }
);

export const actions = {
  getTotalOrders: count => (
    { type: actionTypes.getTotalOrders, payload: { count } }),
  getPendingOrders: count => (
    { type: actionTypes.getPendingOrders, payload: { count } }),
  getApprovedOrders: count => (
    { type: actionTypes.getApprovedOrders, payload: { count } }),
  getCompletedOrders: count => (
    { type: actionTypes.getCompletedOrders, payload: { count } }),
  getRefundedOrders: count => (
    { type: actionTypes.getRefundedOrders, payload: { count } }),
  getUnpaidOrders: count => (
    { type: actionTypes.getUnpaidOrders, payload: { count } }),
  getTotalUsers: count => (
    { type: actionTypes.getTotalUsers, payload: { count } }),
  getTotalAffiliates: count => (
    { type: actionTypes.getTotalAffiliates, payload: { count } }),
};

// export function* saga() {
//   yield takeLatest(actionTypes.Login, function* loginSaga() {
//     yield put(actions.requestUser());
//   });

//   yield takeLatest(actionTypes.Register, function* registerSaga() {
//     yield put(actions.requestUser());
//   });

//   yield takeLatest(actionTypes.UserRequested, function* userRequested() {
//     const { data: user } = yield getUserByToken();

//     yield put(actions.fulfillUser(user));
//   });
// }
