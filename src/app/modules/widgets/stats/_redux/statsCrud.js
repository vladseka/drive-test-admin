import axios from "axios";

export const GET_COUNT_ALL_ORDER = process.env.REACT_APP_SERVER_URL + "/api/orders/count/all";
export const GET_COUNT_PENDING_ORDER = process.env.REACT_APP_SERVER_URL + "/api/orders/count/status/0";
export const GET_COUNT_APPROVED_ORDER = process.env.REACT_APP_SERVER_URL + "/api/orders/count/status/1";
export const GET_COUNT_COMPLETED_ORDER = process.env.REACT_APP_SERVER_URL + "/api/orders/count/status/2";
export const GET_COUNT_REFUNDED_ORDER = process.env.REACT_APP_SERVER_URL + "/api/orders/count/status/3";
export const GET_COUNT_UNPAID_ORDER = process.env.REACT_APP_SERVER_URL + "/api/orders/count/status/4";
export const GET_COUNT_TOTAL_USERS = process.env.REACT_APP_SERVER_URL + "/api/users/count";
export const GET_COUNT_TOTAL_AFFILIATES = process.env.REACT_APP_SERVER_URL + "/api/affiliate/count";

export function getCountAllOrder() {
  // Authorization head should be fulfilled in interceptor.
  return axios.get(GET_COUNT_ALL_ORDER);
}

export function getCountPendingOrder() {
  // Authorization head should be fulfilled in interceptor.
  return axios.get(GET_COUNT_PENDING_ORDER);
}

export function getCountApprovedOrder() {
  // Authorization head should be fulfilled in interceptor.
  return axios.get(GET_COUNT_APPROVED_ORDER);
}
export function getCountCompletedOrder() {
  // Authorization head should be fulfilled in interceptor.
  return axios.get(GET_COUNT_COMPLETED_ORDER);
}
export function getCountRefundedOrder() {
  // Authorization head should be fulfilled in interceptor.
  return axios.get(GET_COUNT_REFUNDED_ORDER);
}
export function getCountUnpaidOrder() {
  // Authorization head should be fulfilled in interceptor.
  return axios.get(GET_COUNT_UNPAID_ORDER);
}
export function getCountTotalUsers() {
  // Authorization head should be fulfilled in interceptor.
  return axios.get(GET_COUNT_TOTAL_USERS);
}
export function getCountTotalAffiliates() {
  // Authorization head should be fulfilled in interceptor.
  return axios.get(GET_COUNT_TOTAL_AFFILIATES);
}
