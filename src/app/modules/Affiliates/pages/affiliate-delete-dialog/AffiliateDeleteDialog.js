/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../_redux/affiliatesActions";
import { useAffiliatesUIContext } from "../AffiliatesUIContext";

export function AffiliateDeleteDialog({ id, show, onHide }) {
    // Affiliates UI Context
    const affiliatesUIContext = useAffiliatesUIContext();
    const affiliatesUIProps = useMemo(() => {
        return {
            setIds: affiliatesUIContext.setIds,
            queryParams: affiliatesUIContext.queryParams,
        };
    }, [affiliatesUIContext]);

    // Affiliates Redux state
    const dispatch = useDispatch();
    const { isLoading } = useSelector(
        (state) => ({ isLoading: state.affiliates.actionsLoading }),
        shallowEqual
    );

    // if !id we should close modal
    useEffect(() => {
        if (!id) {
            onHide();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [id]);

    // looking for loading/dispatch
    useEffect(() => { }, [isLoading, dispatch]);

    const deleteAffiliate = () => {
        // server request for deleting affiliate by id
        dispatch(actions.deleteAffiliate(id)).then(() => {
            // refresh list after deletion
            dispatch(actions.fetchAffiliates(affiliatesUIProps.queryParams));
            // clear selections list
            affiliatesUIProps.setIds([]);
            // closing delete modal
            onHide();
        });
    };

    return (
        <Modal
            show={show}
            onHide={onHide}
            aria-labelledby="example-modal-sizes-title-lg"
        >
            {isLoading && <ModalProgressBar variant="query" />}
            <Modal.Header closeButton>
                <Modal.Title id="example-modal-sizes-title-lg">
                    Affiliates Delete
        </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {!isLoading && (
                    <span>Are you sure to permanently delete this user?</span>
                )}
                {isLoading && <span>User is deleting...</span>}
            </Modal.Body>
            <Modal.Footer>
                <div>
                    <button
                        type="button"
                        onClick={onHide}
                        className="btn btn-light btn-elevate"
                    >
                        Cancel
          </button>
                    <> </>
                    <button
                        type="button"
                        onClick={deleteAffiliate}
                        className="btn btn-delete btn-elevate"
                    >
                        Delete
          </button>
                </div>
            </Modal.Footer>
        </Modal>
    );
}
