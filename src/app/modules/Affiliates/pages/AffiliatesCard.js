import React from "react";
import {
    Card,
    CardBody,
    CardHeader,
    CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { AffiliatesTable } from "./affiliates-table/AffiliatesTable";

export function AffiliatesCard() {

    return (
        <Card>
            <CardHeader title="Affiliates list">
                <CardHeaderToolbar>
                    {/* <button
                        type="button"
                        className="btn btn-primary"
                        onClick={affiliatesUIProps.newAffiliateButtonClick}
                    >
                        New Affiliate
          </button> */}
                </CardHeaderToolbar>
            </CardHeader>
            <CardBody>
                <AffiliatesTable />
            </CardBody>
        </Card>
    );
}
