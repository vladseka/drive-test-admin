// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
    PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../_redux/affiliatesActions";
import * as uiHelpers from "../AffiliatesUIHelpers";
import {
    // getSelectRow,
    getHandlerTableChange,
    NoRecordsFoundMessage,
    PleaseWaitMessage,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useAffiliatesUIContext } from "../AffiliatesUIContext";

export function AffiliatesTable() {
    // Affiliates UI Context
    const affiliatesUIContext = useAffiliatesUIContext();
    const affiliatesUIProps = useMemo(() => {
        return {
            ids: affiliatesUIContext.ids,
            setIds: affiliatesUIContext.setIds,
            queryParams: affiliatesUIContext.queryParams,
            setQueryParams: affiliatesUIContext.setQueryParams,
            openDeleteAffiliateDialog: affiliatesUIContext.openDeleteAffiliateDialog,
        };
    }, [affiliatesUIContext]);

    // Getting curret state of affiliates list from store (Redux)
    const { currentState } = useSelector(
        (state) => ({ currentState: state.affiliates }),
        shallowEqual
    );
    const { totalCount, entities, listLoading } = currentState;
    // Affiliates Redux state
    const dispatch = useDispatch();
    useEffect(() => {
        // clear selections list
        affiliatesUIProps.setIds([]);
        // server call by queryParams
        dispatch(actions.fetchAffiliates(affiliatesUIProps.queryParams));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [affiliatesUIProps.queryParams, dispatch]);
    // Table columns
    const columns = [
        {
            dataField: "email",
            text: "Email",
        },
        {
            dataField: "url",
            text: "Affiliate URL",
            formatter: columnFormatters.UrlColumnFormatter,
        },
        {
            dataField: "action",
            text: "Actions",
            formatter: columnFormatters.ActionsColumnFormatter,
            formatExtraData: {
                openDeleteAffiliateDialog: affiliatesUIProps.openDeleteAffiliateDialog,
            },
            classes: "text-right pr-0",
            headerClasses: "text-right pr-3",
            style: {
                minWidth: "100px",
            },
        },
    ];
    // Table pagination properties
    const paginationOptions = {
        custom: true,
        totalSize: totalCount,
        sizePerPageList: uiHelpers.sizePerPageList,
        sizePerPage: affiliatesUIProps.queryParams.pageSize,
        page: affiliatesUIProps.queryParams.pageNumber,
    };
    return (
        <>
            <PaginationProvider pagination={paginationFactory(paginationOptions)}>
                {({ paginationProps, paginationTableProps }) => {
                    return (
                        <Pagination
                            isLoading={listLoading}
                            paginationProps={paginationProps}
                        >
                            <BootstrapTable
                                wrapperClasses="table-responsive"
                                classes="table table-head-custom table-borderless table-vertical-center"
                                bootstrap4
                                baffiliateed={false}
                                remote
                                keyField="id"
                                data={entities === null ? [] : entities}
                                columns={columns}
                                defaultSorted={uiHelpers.defaultSorted}
                                onTableChange={getHandlerTableChange(
                                    affiliatesUIProps.setQueryParams
                                )}
                                // selectRow={getSelectRow({
                                //     entities,
                                //     ids: affiliatesUIProps.ids,
                                //     setIds: affiliatesUIProps.setIds,
                                // })}
                                {...paginationTableProps}
                            >
                                <PleaseWaitMessage entities={entities} />
                                <NoRecordsFoundMessage entities={entities} />
                            </BootstrapTable>
                        </Pagination>
                    );
                }}
            </PaginationProvider>
        </>
    );
}
