import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./AffiliatesUIHelpers";

const AffiliatesUIContext = createContext();

export function useAffiliatesUIContext() {
    return useContext(AffiliatesUIContext);
}

export const AffiliatesUIConsumer = AffiliatesUIContext.Consumer;

export function AffiliatesUIProvider({ affiliatesUIEvents, children }) {
    const [queryParams, setQueryParamsBase] = useState(initialFilter);
    const [ids, setIds] = useState([]);
    const setQueryParams = useCallback((nextQueryParams) => {
        setQueryParamsBase((prevQueryParams) => {
            if (isFunction(nextQueryParams)) {
                nextQueryParams = nextQueryParams(prevQueryParams);
            }

            if (isEqual(prevQueryParams, nextQueryParams)) {
                return prevQueryParams;
            }

            return nextQueryParams;
        });
    }, []);

    const value = {
        queryParams,
        setQueryParamsBase,
        ids,
        setIds,
        setQueryParams,
        openDeleteAffiliateDialog: affiliatesUIEvents.openDeleteAffiliateDialog,
    };

    return (
        <AffiliatesUIContext.Provider value={value}>
            {children}
        </AffiliatesUIContext.Provider>
    );
}
