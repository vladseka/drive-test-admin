import React from "react";
import { Route } from "react-router-dom";
import { AffiliatesLoadingDialog } from "./affiliates-loading-dialog/AffiliatesLoadingDialog";
import { AffiliateDeleteDialog } from "./affiliate-delete-dialog/AffiliateDeleteDialog";
import { AffiliatesCard } from "./AffiliatesCard";
import { AffiliatesUIProvider } from "./AffiliatesUIContext";

export function AffiliatesPage({ history }) {
    const affiliatesUIEvents = {
        openDeleteAffiliateDialog: (id) => {
            history.push(`/affiliates/${id}/delete`);
        },
    };

    return (
        <AffiliatesUIProvider affiliatesUIEvents={affiliatesUIEvents}>
            <AffiliatesLoadingDialog />
            <Route path="/affiliates/:id/delete">
                {({ history, match }) => (
                    <AffiliateDeleteDialog
                        show={match != null}
                        id={match && match.params.id}
                        onHide={() => {
                            history.push("/affiliates");
                        }}
                    />
                )}
            </Route>
            <AffiliatesCard />
        </AffiliatesUIProvider>
    );
}
