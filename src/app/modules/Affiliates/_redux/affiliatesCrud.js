import axios from "axios";

export const AFFILIATES_URL = process.env.REACT_APP_SERVER_URL + "/api/affiliate";

// CREATE =>  POST: add a new affiliate to the server
export function createAffiliate(affiliate) {
    return axios.post(AFFILIATES_URL, { affiliate });
}

// READ
export function getAllAffiliates() {
    return axios.get(AFFILIATES_URL);
}

export function getAffiliateById(affiliateId) {
    return axios.get(`${AFFILIATES_URL}/${affiliateId}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findAffiliates(queryParams) {
    return axios.post(`${AFFILIATES_URL}/find`, { queryParams });
}

// UPDATE => PUT: update the procuct on the server
export function updateAffiliate(affiliate) {
    return axios.put(`${AFFILIATES_URL}/${affiliate._id}`, { affiliate });
}

// DELETE => delete the affiliate from the server
export function deleteAffiliate(affiliateId) {
    return axios.delete(`${AFFILIATES_URL}/${affiliateId}`);
}
