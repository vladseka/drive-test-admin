import * as requestFromServer from "./affiliatesCrud";
import { affiliatesSlice, callTypes } from "./affiliatesSlice";

const { actions } = affiliatesSlice;

export const fetchAffiliates = queryParams => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.list }));
    return requestFromServer
        .findAffiliates(queryParams)
        .then(response => {
            const { totalCount, entities } = response.data;
            dispatch(actions.affiliatesFetched({ totalCount, entities }));
        })
        .catch(error => {
            error.clientMessage = "Can't find affiliates";
            dispatch(actions.catchError({ error, callType: callTypes.list }));
        });
};

export const fetchAffiliate = id => dispatch => {
    if (!id) {
        return dispatch(actions.affiliateFetched({ affiliateForEdit: undefined }));
    }

    dispatch(actions.startCall({ callType: callTypes.action }));
    return requestFromServer
        .getAffiliateById(id)
        .then(response => {
            const affiliate = response.data;
            affiliate.id = affiliate._id;
            dispatch(actions.affiliateFetched({ affiliateForEdit: affiliate }));
        })
        .catch(error => {
            error.clientMessage = "Can't find affiliate";
            dispatch(actions.catchError({ error, callType: callTypes.action }));
        });
};

export const deleteAffiliate = id => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.action }));
    return requestFromServer
        .deleteAffiliate(id)
        .then(() => {
            dispatch(actions.affiliateDeleted({ id }));
        })
        .catch(error => {
            error.clientMessage = "Can't delete affiliate";
            dispatch(actions.catchError({ error, callType: callTypes.action }));
        });
};

export const createAffiliate = affiliateForCreation => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.action }));
    return requestFromServer
        .createAffiliate(affiliateForCreation)
        .then(response => {
            const { affiliate } = response.data;
            dispatch(actions.affiliateCreated({ affiliate }));
        })
        .catch(error => {
            error.clientMessage = "Can't create affiliate";
            dispatch(actions.catchError({ error, callType: callTypes.action }));
        });
};

export const updateAffiliate = affiliate => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.action }));
    return requestFromServer
        .updateAffiliate(affiliate)
        .then(() => {
            dispatch(actions.affiliateUpdated({ affiliate }));
        })
        .catch(error => {
            error.clientMessage = "Can't update affiliate";
            dispatch(actions.catchError({ error, callType: callTypes.action }));
        });
};
