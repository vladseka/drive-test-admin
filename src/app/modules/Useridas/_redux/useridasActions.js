import * as requestFromServer from "./useridasCrud";
import { useridasSlice, callTypes } from "./useridasSlice";

const { actions } = useridasSlice;

export const fetchUseridas = queryParams => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.list }));
    return requestFromServer
        .findUseridas(queryParams)
        .then(response => {
            const { totalCount, entities } = response.data;
            dispatch(actions.useridasFetched({ totalCount, entities }));
        })
        .catch(error => {
            error.clientMessage = "Can't find useridas";
            dispatch(actions.catchError({ error, callType: callTypes.list }));
        });
};

export const fetchUserida = id => dispatch => {
    if (!id) {
        return dispatch(actions.useridaFetched({ useridaForEdit: undefined }));
    }

    dispatch(actions.startCall({ callType: callTypes.action }));
    return requestFromServer
        .getUseridaById(id)
        .then(response => {
            const userida = response.data;
            userida.id = userida._id;
            dispatch(actions.useridaFetched({ useridaForEdit: userida }));
        })
        .catch(error => {
            error.clientMessage = "Can't find userida";
            dispatch(actions.catchError({ error, callType: callTypes.action }));
        });
};

export const deleteUserida = id => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.action }));
    return requestFromServer
        .deleteUserida(id)
        .then(() => {
            dispatch(actions.useridaDeleted({ id }));
        })
        .catch(error => {
            error.clientMessage = "Can't delete userida";
            dispatch(actions.catchError({ error, callType: callTypes.action }));
        });
};

export const createUserida = useridaForCreation => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.action }));
    return requestFromServer
        .createUserida(useridaForCreation)
        .then(response => {
            const { userida } = response.data;
            dispatch(actions.useridaCreated({ userida }));
        })
        .catch(error => {
            error.clientMessage = "Can't create userida";
            dispatch(actions.catchError({ error, callType: callTypes.action }));
        });
};

export const updateUserida = userida => dispatch => {
    dispatch(actions.startCall({ callType: callTypes.action }));
    return requestFromServer
        .updateUserida(userida)
        .then(() => {
            dispatch(actions.useridaUpdated({ userida }));
        })
        .catch(error => {
            error.clientMessage = "Can't update userida";
            dispatch(actions.catchError({ error, callType: callTypes.action }));
        });
};
