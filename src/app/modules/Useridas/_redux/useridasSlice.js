import { createSlice } from "@reduxjs/toolkit";

const initialUseridasState = {
    listLoading: false,
    actionsLoading: false,
    totalCount: 0,
    entities: null,
    useridaForEdit: undefined,
    lastError: null
};
export const callTypes = {
    list: "list",
    action: "action"
};

export const useridasSlice = createSlice({
    name: "useridas",
    initialState: initialUseridasState,
    reducers: {
        catchError: (state, action) => {
            state.error = `${action.type}: ${action.payload.error}`;
            if (action.payload.callType === callTypes.list) {
                state.listLoading = false;
            } else {
                state.actionsLoading = false;
            }
        },
        startCall: (state, action) => {
            state.error = null;
            if (action.payload.callType === callTypes.list) {
                state.listLoading = true;
            } else {
                state.actionsLoading = true;
            }
        },
        // getUseridaById
        useridaFetched: (state, action) => {
            state.actionsLoading = false;
            state.useridaForEdit = action.payload.useridaForEdit;
            state.error = null;
        },
        // findUseridas
        useridasFetched: (state, action) => {
            const { totalCount, entities } = action.payload;
            state.listLoading = false;
            state.error = null;
            state.entities = entities;
            state.totalCount = totalCount;
        },
        // createUserida
        useridaCreated: (state, action) => {
            state.actionsLoading = false;
            state.error = null;
            state.entities.push(action.payload.userida);
        },
        // updateUserida
        useridaUpdated: (state, action) => {
            state.error = null;
            state.actionsLoading = false;
            state.entities = state.entities.map(entity => {
                if (entity.id === action.payload.userida._id) {
                    return action.payload.userida;
                }
                return entity;
            });
        },
        // deleteUserida
        useridaDeleted: (state, action) => {
            state.error = null;
            state.actionsLoading = false;
            state.entities = state.entities.filter(el => el.id !== action.payload.id);
        }
    }
});
