import axios from "axios";

export const USERIDAS_URL = process.env.REACT_APP_SERVER_URL + "/api/users";

// CREATE =>  POST: add a new userida to the server
export function createUserida(userida) {
    return axios.post(USERIDAS_URL, { userida });
}

// READ
export function getAllUseridas() {
    return axios.get(USERIDAS_URL);
}

export function getUseridaById(useridaId) {
    return axios.get(`${USERIDAS_URL}/${useridaId}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findUseridas(queryParams) {
    return axios.post(`${USERIDAS_URL}/find`, { queryParams });
}

// UPDATE => PUT: update the procuct on the server
export function updateUserida(userida) {
    return axios.put(`${USERIDAS_URL}/${userida._id}`, { userida });
}

// DELETE => delete the userida from the server
export function deleteUserida(useridaId) {
    return axios.delete(`${USERIDAS_URL}/${useridaId}`);
}
