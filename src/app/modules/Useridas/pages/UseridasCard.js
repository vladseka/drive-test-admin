import React from "react";
import {
    Card,
    CardBody,
    CardHeader,
    CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { UseridasTable } from "./useridas-table/UseridasTable";

export function UseridasCard() {

    return (
        <Card>
            <CardHeader title="IDA user list">
                <CardHeaderToolbar>
                    {/* <button
                        type="button"
                        className="btn btn-primary"
                        onClick={useridasUIProps.newUseridaButtonClick}
                    >
                        New Userida
          </button> */}
                </CardHeaderToolbar>
            </CardHeader>
            <CardBody>
                <UseridasTable />
            </CardBody>
        </Card>
    );
}
