// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
    PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../_redux/useridasActions";
import * as uiHelpers from "../UseridasUIHelpers";
import {
    // getSelectRow,
    getHandlerTableChange,
    NoRecordsFoundMessage,
    PleaseWaitMessage,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useUseridasUIContext } from "../UseridasUIContext";

export function UseridasTable() {
    // Useridas UI Context
    const useridasUIContext = useUseridasUIContext();
    const useridasUIProps = useMemo(() => {
        return {
            ids: useridasUIContext.ids,
            setIds: useridasUIContext.setIds,
            queryParams: useridasUIContext.queryParams,
            setQueryParams: useridasUIContext.setQueryParams,
            openEditUseridaPage: useridasUIContext.openEditUseridaPage,
            openDeleteUseridaDialog: useridasUIContext.openDeleteUseridaDialog,
        };
    }, [useridasUIContext]);

    // Getting curret state of useridas list from store (Redux)
    const { currentState } = useSelector(
        (state) => ({ currentState: state.useridas }),
        shallowEqual
    );
    const { totalCount, entities, listLoading } = currentState;
    // Useridas Redux state
    const dispatch = useDispatch();
    useEffect(() => {
        // clear selections list
        useridasUIProps.setIds([]);
        // server call by queryParams
        dispatch(actions.fetchUseridas(useridasUIProps.queryParams));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [useridasUIProps.queryParams, dispatch]);
    // Table columns
    const columns = [
        {
            dataField: "name",
            text: "Name",
        },
        {
            dataField: "email",
            text: "Email",
        },
        {
            dataField: "phone",
            text: "Mobile No.",
        },
        {
            dataField: "company",
            text: "Company",
        },
        {
            dataField: "action",
            text: "Actions",
            formatter: columnFormatters.ActionsColumnFormatter,
            formatExtraData: {
                openEditUseridaPage: useridasUIProps.openEditUseridaPage,
                openDeleteUseridaDialog: useridasUIProps.openDeleteUseridaDialog,
            },
            classes: "text-right pr-0",
            headerClasses: "text-right pr-3",
            style: {
                minWidth: "100px",
            },
        },
    ];
    // Table pagination properties
    const paginationOptions = {
        custom: true,
        totalSize: totalCount,
        sizePerPageList: uiHelpers.sizePerPageList,
        sizePerPage: useridasUIProps.queryParams.pageSize,
        page: useridasUIProps.queryParams.pageNumber,
    };
    return (
        <>
            <PaginationProvider pagination={paginationFactory(paginationOptions)}>
                {({ paginationProps, paginationTableProps }) => {
                    return (
                        <Pagination
                            isLoading={listLoading}
                            paginationProps={paginationProps}
                        >
                            <BootstrapTable
                                wrapperClasses="table-responsive"
                                classes="table table-head-custom table-borderless table-vertical-center"
                                bootstrap4
                                buseridaed={false}
                                remote
                                keyField="id"
                                data={entities === null ? [] : entities}
                                columns={columns}
                                defaultSorted={uiHelpers.defaultSorted}
                                onTableChange={getHandlerTableChange(
                                    useridasUIProps.setQueryParams
                                )}
                                // selectRow={getSelectRow({
                                //     entities,
                                //     ids: useridasUIProps.ids,
                                //     setIds: useridasUIProps.setIds,
                                // })}
                                {...paginationTableProps}
                            >
                                <PleaseWaitMessage entities={entities} />
                                <NoRecordsFoundMessage entities={entities} />
                            </BootstrapTable>
                        </Pagination>
                    );
                }}
            </PaginationProvider>
        </>
    );
}
