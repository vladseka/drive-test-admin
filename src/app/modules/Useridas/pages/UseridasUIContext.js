import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./UseridasUIHelpers";

const UseridasUIContext = createContext();

export function useUseridasUIContext() {
    return useContext(UseridasUIContext);
}

export const UseridasUIConsumer = UseridasUIContext.Consumer;

export function UseridasUIProvider({ useridasUIEvents, children }) {
    const [queryParams, setQueryParamsBase] = useState(initialFilter);
    const [ids, setIds] = useState([]);
    const setQueryParams = useCallback((nextQueryParams) => {
        setQueryParamsBase((prevQueryParams) => {
            if (isFunction(nextQueryParams)) {
                nextQueryParams = nextQueryParams(prevQueryParams);
            }

            if (isEqual(prevQueryParams, nextQueryParams)) {
                return prevQueryParams;
            }

            return nextQueryParams;
        });
    }, []);

    const value = {
        queryParams,
        setQueryParamsBase,
        ids,
        setIds,
        setQueryParams,
        newUseridaButtonClick: useridasUIEvents.newUseridaButtonClick,
        openEditUseridaPage: useridasUIEvents.openEditUseridaPage,
        openDeleteUseridaDialog: useridasUIEvents.openDeleteUseridaDialog,
    };

    return (
        <UseridasUIContext.Provider value={value}>
            {children}
        </UseridasUIContext.Provider>
    );
}
