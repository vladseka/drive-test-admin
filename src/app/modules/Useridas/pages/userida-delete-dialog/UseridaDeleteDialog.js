/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../_redux/useridasActions";
import { useUseridasUIContext } from "../UseridasUIContext";

export function UseridaDeleteDialog({ id, show, onHide }) {
    // Useridas UI Context
    const useridasUIContext = useUseridasUIContext();
    const useridasUIProps = useMemo(() => {
        return {
            setIds: useridasUIContext.setIds,
            queryParams: useridasUIContext.queryParams,
        };
    }, [useridasUIContext]);

    // Useridas Redux state
    const dispatch = useDispatch();
    const { isLoading } = useSelector(
        (state) => ({ isLoading: state.useridas.actionsLoading }),
        shallowEqual
    );

    // if !id we should close modal
    useEffect(() => {
        if (!id) {
            onHide();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [id]);

    // looking for loading/dispatch
    useEffect(() => { }, [isLoading, dispatch]);

    const deleteUserida = () => {
        // server request for deleting userida by id
        dispatch(actions.deleteUserida(id)).then(() => {
            // refresh list after deletion
            dispatch(actions.fetchUseridas(useridasUIProps.queryParams));
            // clear selections list
            useridasUIProps.setIds([]);
            // closing delete modal
            onHide();
        });
    };

    return (
        <Modal
            show={show}
            onHide={onHide}
            aria-labelledby="example-modal-sizes-title-lg"
        >
            {isLoading && <ModalProgressBar variant="query" />}
            <Modal.Header closeButton>
                <Modal.Title id="example-modal-sizes-title-lg">
                    IDA user Delete
        </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {!isLoading && (
                    <span>Are you sure to permanently delete this user?</span>
                )}
                {isLoading && <span>User is deleting...</span>}
            </Modal.Body>
            <Modal.Footer>
                <div>
                    <button
                        type="button"
                        onClick={onHide}
                        className="btn btn-light btn-elevate"
                    >
                        Cancel
          </button>
                    <> </>
                    <button
                        type="button"
                        onClick={deleteUserida}
                        className="btn btn-delete btn-elevate"
                    >
                        Delete
          </button>
                </div>
            </Modal.Footer>
        </Modal>
    );
}
