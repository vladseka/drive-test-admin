/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../_redux/useridasActions";
import {
    Card,
    CardBody,
    CardHeader,
    CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { UseridaEditForm } from "./UseridaEditForm";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";

const initUserida = {
    id: undefined,
    name: "",
    email: "",
    phone: "",
    company: ""
};

export function UseridaEdit({
    history,
    match: {
        params: { id },
    },
}) {
    // Subheader
    const suhbeader = useSubheader();

    // Tabs
    const [tab, setTab] = useState("basic");
    const [title, setTitle] = useState("");
    const dispatch = useDispatch();
    // const layoutDispatch = useContext(LayoutContext.Dispatch);
    const { actionsLoading, useridaForEdit } = useSelector(
        (state) => ({
            actionsLoading: state.useridas.actionsLoading,
            useridaForEdit: state.useridas.useridaForEdit,
        }),
        shallowEqual
    );

    useEffect(() => {
        dispatch(actions.fetchUserida(id));
    }, [id, dispatch]);

    useEffect(() => {
        let _title = id ? "" : "New IDA User";
        if (useridaForEdit && id) {
            _title = `Edit IDA User '${useridaForEdit.name} - ${useridaForEdit.phone}'`;
        }

        setTitle(_title);
        suhbeader.setTitle(_title);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [useridaForEdit, id]);

    const saveUserida = (values) => {
        if (!id) {
            dispatch(actions.createUserida(values)).then(() => backToUseridasList());
        } else {
            dispatch(actions.updateUserida(values)).then(() => backToUseridasList());
        }
    };

    const btnRef = useRef();
    const saveUseridaClick = () => {
        if (btnRef && btnRef.current) {
            btnRef.current.click();
        }
    };

    const backToUseridasList = () => {
        history.push(`/useridas`);
    };

    return (
        <Card>
            {actionsLoading && <ModalProgressBar />}
            <CardHeader title={title}>
                <CardHeaderToolbar>
                    <button
                        type="button"
                        onClick={backToUseridasList}
                        className="btn btn-light"
                    >
                        <i className="fa fa-arrow-left"></i>
            Back
          </button>
                    {`  `}
                    <button
                        type="submit"
                        className="btn btn-primary ml-2"
                        onClick={saveUseridaClick}
                    >
                        Save
          </button>
                </CardHeaderToolbar>
            </CardHeader>
            <CardBody>
                <ul className="nav nav-tabs nav-tabs-line " role="tablist">
                    <li className="nav-item" onClick={() => setTab("basic")}>
                        <a
                            className={`nav-link ${tab === "basic" && "active"}`}
                            data-toggle="tab"
                            role="tab"
                            aria-selected={(tab === "basic").toString()}
                        >
                            Basic info
            </a>
                    </li>
                </ul>
                <div className="mt-5">
                    {tab === "basic" && (
                        <UseridaEditForm
                            actionsLoading={actionsLoading}
                            userida={useridaForEdit || initUserida}
                            btnRef={btnRef}
                            saveUserida={saveUserida}
                        />
                    )}
                </div>
            </CardBody>
        </Card>
    );
}
