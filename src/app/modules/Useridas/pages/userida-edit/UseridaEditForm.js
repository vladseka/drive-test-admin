// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";

// Validation schema
const UseridaEditSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, "Minimum 2 symbols")
    .max(30, "Maximum 30 symbols")
    .required("Name is required"),
  email: Yup.string()
    .min(4, "Minimum 4 symbols")
    .max(30, "Maximum 30 symbols")
    .required("Email is required"),
  phone: Yup.string()
    .min(7, "Minimum 7 symbols")
    .max(16, "Maximum 16 symbols")
    .required("Mobile number is required"),
  company: Yup.string()
    .min(2, "Minimum 2 symbols")
    .max(18, "Maximum 18 symbols")
    .required("Company name is required")
});

export function UseridaEditForm({
  userida,
  btnRef,
  saveUserida,
}) {
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={userida}
        validationSchema={UseridaEditSchema}
        onSubmit={(values) => {
          saveUserida(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    name="name"
                    component={Input}
                    placeholder="Name"
                    label="Name"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    name="email"
                    component={Input}
                    placeholder="Email"
                    label="Email"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    name="phone"
                    component={Input}
                    placeholder="Phone"
                    label="Phone"
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    name="company"
                    component={Input}
                    placeholder="Company"
                    label="Company"
                  />
                </div>
              </div>
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
