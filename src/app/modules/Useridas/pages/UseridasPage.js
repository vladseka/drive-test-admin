import React from "react";
import { Route } from "react-router-dom";
import { UseridasLoadingDialog } from "./useridas-loading-dialog/UseridasLoadingDialog";
import { UseridaDeleteDialog } from "./userida-delete-dialog/UseridaDeleteDialog";
import { UseridaEdit } from "./userida-edit/UseridaEdit";
import { UseridasCard } from "./UseridasCard";
import { UseridasUIProvider } from "./UseridasUIContext";

export function UseridasPage({ history }) {
    const useridasUIEvents = {
        newUseridaButtonClick: () => {
            history.push("/useridas/new");
        },
        openEditUseridaPage: (id) => {
            history.push(`/useridas/${id}/edit`);
        },
        openDeleteUseridaDialog: (id) => {
            history.push(`/useridas/${id}/delete`);
        },
    };

    return (
        <UseridasUIProvider useridasUIEvents={useridasUIEvents}>
            <UseridasLoadingDialog />
            <Route path="/useridas/:id/delete">
                {({ history, match }) => (
                    <UseridaDeleteDialog
                        show={match != null}
                        id={match && match.params.id}
                        onHide={() => {
                            history.push("/useridas");
                        }}
                    />
                )}
            </Route>
            <Route path="/useridas/:id/edit" component={UseridaEdit} />
            <UseridasCard />
        </UseridasUIProvider>
    );
}
