import React, { Suspense } from "react";
import { Redirect, Switch } from "react-router-dom";
import { LayoutSplashScreen, ContentRoute } from "../_metronic/layout";
import { BuilderPage } from "./pages/BuilderPage";
import { MyPage } from "./pages/MyPage";
import { OrdersPage } from "./modules/Orders/pages/OrdersPage";
import { RefundsPage } from "./modules/Refunds/pages/RefundsPage";
import { UnpaidsPage } from "./modules/Unpaid/pages/UnpaidsPage";
import { UseridasPage } from "./modules/Useridas/pages/UseridasPage";
import { AffiliatesPage } from "./modules/Affiliates/pages/AffiliatesPage";
import { DashboardPage } from "./pages/DashboardPage";

export default function BasePage() {
  // useEffect(() => {
  //   console.log('Base page');
  //   const authToken = useSelector(state => state.auth.authToken)

  // }, []) // [] - is required if you need only one call
  // https://reactjs.org/docs/hooks-reference.html#useeffect

  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        {
          /* Redirect from root URL to /dashboard. */
          <Redirect exact from="/" to="/dashboard" />
        }
        <ContentRoute path="/dashboard" component={DashboardPage} />
        <ContentRoute path="/builder" component={BuilderPage} />
        <ContentRoute path="/my-page" component={MyPage} />
        <ContentRoute path="/orders/:status" component={OrdersPage} />
        <ContentRoute path="/orders" component={OrdersPage} />
        <ContentRoute path="/refunds" component={RefundsPage} />
        <ContentRoute path="/unpaids" component={UnpaidsPage} />
        <ContentRoute path="/useridas" component={UseridasPage} />
        <ContentRoute path="/affiliates" component={AffiliatesPage} />
        <Redirect to="error/error-v1" />
      </Switch>
    </Suspense>
  );
}
