import { all } from "redux-saga/effects";
import { combineReducers } from "redux";

import * as auth from "../app/modules/Auth/_redux/authRedux";
import { ordersSlice } from "../app/modules/Orders/_redux/ordersSlice";
import { refundsSlice } from "../app/modules/Refunds/_redux/refundsSlice";
import { unpaidsSlice } from "../app/modules/Unpaid/_redux/unpaidsSlice";
import { useridasSlice } from "../app/modules/Useridas/_redux/useridasSlice";
import { affiliatesSlice } from "../app/modules/Affiliates/_redux/affiliatesSlice";
import * as stats from "../app/modules/widgets/stats/_redux/statsRedux";
import { smssSlice } from "../app/modules/Orders/_redux/smss/smssSlice";

export const rootReducer = combineReducers({
  auth: auth.reducer,
  orders: ordersSlice.reducer,
  refunds: refundsSlice.reducer,
  unpaids: unpaidsSlice.reducer,
  useridas: useridasSlice.reducer,
  affiliates: affiliatesSlice.reducer,
  stats: stats.reducer,
  smss: smssSlice.reducer,
});

export function* rootSaga() {
  yield all([auth.saga()]);
}
